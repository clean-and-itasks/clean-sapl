_feval = function (expr) { // check on length split for == and <=
		var y, f, xs;
		while (1) {
			if (typeof (expr) == "object") {
				if (expr.length == 1){
					expr = expr[0];
				}else if (typeof (expr[0]) == "function") { // closure
					f = expr[0];
					xs = expr[1];
					if (f.length == xs.length) { // most often occuring case
					
						// unfold embedded arrays
						y = f.apply(null, xs);
						while(typeof (y) == "object" && y.length == 1) {
							y = y[0];
						}
												
						expr[0] = y;
						expr.length = 1;
						expr = y;
					} else if (f.length < xs.length) { // less likely case
						y = f.apply(null, xs.splice(0, f.length));
						expr[0] = y;
					} else // not enough args
					return expr;
				} else if (typeof (expr[0]) == "object") { // curried application -> uncurry
					y = expr[0];
					expr[0] = y[0];
					expr[1] = y[1].concat(expr[1]);
				} else return expr; // constructor
			} else if (typeof (expr) == "function"){
				expr = [expr, []];
			}
			else // simple value
			return expr;
		}
	};

// TODO: Create a thunk and use _feval, a proper implementation is needed...
_fapp = function (f, xs){
	return _feval([f,xs]);
}
