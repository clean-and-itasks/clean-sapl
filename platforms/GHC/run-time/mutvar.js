function _writeMutVar(mv,val,st){
	mv.x = val;
	return st;
}

function _atomicModifyMutVar(mv,f,st){
	var pair = Sapl.apply(f,[mv.x]);
	mv.x = pair[2];
	return __GHC_Tuple_$28$2C$29(st, pair[3]);
}

function _casMutVar(mv,o,n,st){
	if(mv.val === o) {
		var status = 0;
        var r = mv.x;
        mv.x = n;
    } else {
        status = 1;
        r = mv.x;
    }
	return __GHC_Tuple_$28$2C$2C$29(st, status, r);	
}