function __gettimeofday(tv, tz) {
	var now = Date.now();
	tv.v.i32[tv.off/4 + 0] = (now / 1000)|0;
	tv.v.i32[tv.off/4 + 1] = ((now % 1000) * 1000)|0;
	if (tv.size >= tv.off + 12){
		tv.v.i32[tv.off/4 + 2] = ((now % 1000) * 1000)|0;
	}
	return 0;
}

function __gettime(world){
	var d = new Date();
	var n = d.getTime(); 	
	return __GHC_Base_returnIO(__GHC_Types_I$23(n), world);
}

_tz1 = __newByteArray(4);
_tz1.v.w8[0]='P'.charCodeAt();
_tz1.v.w8[1]='D'.charCodeAt();
_tz1.v.w8[2]='T'.charCodeAt();
_tz1.v.w8[3]=0;
_tz2 = __newByteArray(4);
_tz2.v.i8[0]='P'.charCodeAt();
_tz2.v.i8[1]='S'.charCodeAt();
_tz2.v.i8[2]='T'.charCodeAt();
_tz2.v.i8[3]=0;

function __get_current_timezone_seconds(t, pdst, pname){

	var d = new Date();
	var tzo = d.getTimezoneOffset();
	var fy = d.getFullYear();
	
	var jan = new Date(fy, 0, 1);
	var jul = new Date(fy, 6, 1);
	var std_tzo = Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
	
	var is_dst = tzo<std_tzo;

	pdst.v.i32[pdst.off/4 + 0] = is_dst ? 1 : 0;
	pname.v.addr[pname.off] = is_dst ? _tz1 : _tz2;
	return tzo * 60;
}

