// Integer-GMP is really crappy
// Integer: Sign + ByteArray
// Implementation: Integer object + tweaks to behave like a bytearray + sign get/set

function _integerFromWord64(a){
	a = goog.math.Long.fromBits(a.l,a.h);
	return goog.math.Integer.fromString(a.toString());
}

function _integerToWord64(a){
    return {l: a.toInt(), h: a.shiftRight(32).toInt()};
}

function _tweak_integer(iobj){
	iobj.v = {i32: iobj.bits_, w32: iobj.bits_};
	return iobj;
}	

function _getsign(iobj){
	if(iobj.isZero()){
		return 0;
	}else if(iobj.getSign()<0){
		return -1;
	}else{
		return 1;
	}
}

function _sign(x) { return x > 0 ? 1 : x < 0 ? -1 : 0; }

function _setsign(s,iobj){
	if(s == 0){
		return goog.math.Integer.ZERO;
	}else if(_sign(s) != _getsign(iobj)){
		return iobj.negate();
	}else{
		return iobj;
	}
}

function _gcd(a,b){
    var x = a.isNegative()?a.negate():a;
    var y = b.isNegative()?b.negate():b;
    var big, small;
    if(x.compare(y) < 0) {
        small = x;
        big = y;
    } else {
        small = y;
        big = x;
    }
    while(_getsign(small) !== 0) {
        var q = big.divide(small);
        var r = big.subtract(q.multiply(small));
        big = small;
        small = r;
    }
    return big;
}

var _oneOverLog2 = 1 / Math.log(2);

function __integer_cmm_cmpIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	return abits.compare(bbits);
}

function __integer_cmm_cmpIntegerIntzh(sa, abits, i){
	return __integer_cmm_cmpIntegerzh(sa,abits,_sign(i),goog.math.Integer.fromInt(i));
}

function __integer_cmm_plusIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var iobj = _tweak_integer(abits.add(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_minusIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var iobj = _tweak_integer(abits.subtract(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_timesIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var iobj = _tweak_integer(abits.multiply(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_quotRemIntegerzh(sa, abits, sb, bbits) {
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
    var q = _tweak_integer(abits.divide(bbits));
    var r = _tweak_integer(abits.modulo(bbits));
	return __GHC_Tuple_$28$2C$2C$2C$29(_getsign(q),q,_getsign(r),r);
}

function __integer_cmm_quotIntegerzh(sa, abits, sb, bbits) {
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
    var q = _tweak_integer(abits.divide(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(q),q);
}

function __integer_cmm_remIntegerzh(sa, abits, sb, bbits) {
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
    var r = _tweak_integer(abits.modulo(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(r),r);
}

function __integer_cmm_divModIntegerzh(sa, abits, sb, bbits) {
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
    var d = abits.divide(bbits);
    var m = abits.modulo(bbits);
    if(abits.getSign()!==bbits.getSign() && m.getSign() !== 0) {
        d = d.subtract(goog.math.Integer.ONE);
        m = m.add(bbits);
    }	
	return __GHC_Tuple_$28$2C$2C$2C$29(_getsign(d),_tweak_integer(d),_getsign(m),_tweak_integer(m));
}

function __integer_cmm_divIntegerzh(sa, abits, sb, bbits) {
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
    var d = abits.divide(bbits);
    var m = abits.modulo(bbits);
    if(abits.getSign()!==bbits.getSign() && m.getSign() !== 0) {
        d = d.subtract(goog.math.Integer.ONE);
        m = m.add(bbits);
    }		
	return __GHC_Tuple_$28$2C$29(_getsign(d),_tweak_integer(d));
}

function __integer_cmm_modIntegerzh(sa, abits, sb, bbits) {
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
    var d = abits.divide(bbits);
    var m = abits.modulo(bbits);
    if(abits.getSign()!==bbits.getSign() && m.getSign() !== 0) {
        m = m.add(bbits);
    }		
	return __GHC_Tuple_$28$2C$29(_getsign(m),_tweak_integer(m));
}

function __integer_cmm_divExactIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var d = abits.divide(bbits);
	return __GHC_Tuple_$28$2C$29(_getsign(d),_tweak_integer(d));
}

function __integer_cmm_gcdIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var g = _gcd(abits, bbits);
	return __GHC_Tuple_$28$2C$29(_getsign(g),_tweak_integer(g));
}

function __integer_cmm_gcdIntegerIntzh(sa, abits, i){
	abits = _setsign(sa, abits);
	var iobj = goog.math.Integer.fromInt(i);
	var g = _gcd(abits, iobj);
	return g.toInt();
}

function __integer_cmm_gcdIntzh(a, b) {
	var x = a<0 ? -a : a;
	var y = b<0 ? -b : b;
	var big, small;
	if(x<y) {
		small = x;
		big = y;
	} else {
		small = y;
		big = x;
	}
	while(small!==0) {
		var r = big % small;
		big = small;
		small = r;
	}
	return big;
}

function __integer_cmm_decodeDoublezh(x) {
	
    if(isNaN(x)) {
		var iobj = goog.math.Integer.fromInt(3).shiftLeft(51).negate();
		return __GHC_Tuple_$28$2C$2C$29(972,_getsign(iobj),_tweak_integer(iobj));
    }
    if(x < 0) {
        var r = __integer_cmm_decodeDoublezh(-x);
		var n = r[4].negate();
		return __GHC_Tuple_$28$2C$2C$29(r[2],_getsign(n),n);
    }
    if(x === Number.POSITIVE_INFINITY) {
        var iobj = goog.math.Integer.ONE.shiftLeft(52);
		return __GHC_Tuple_$28$2C$2C$29(972,_getsign(iobj),_tweak_integer(iobj));		
    }
	
    var exponent = Math.floor(Math.log(x) * _oneOverLog2)-52;
    var n;
    
	// prevent overflow
    if(exponent < -1000) {
		n = x * Math.pow(2,-exponent-128) * Math.pow(2,128);
    } else if(exponent > 900) {
		n = x * Math.pow(2,-exponent+128) * Math.pow(2,-128);
    } else {
		n = x * Math.pow(2,-exponent);
    }
    
	// fixup precision, do we also need the other way (exponent++) ?
    if(Math.abs(n - Math.floor(n) - 0.5) < 0.0001) {
		exponent--;
		n *= 2;
    }
    	
	var iobj = goog.math.Integer.fromNumber(n);
	return __GHC_Tuple_$28$2C$2C$29(exponent,_getsign(iobj),_tweak_integer(iobj));		
}

function __integer_cmm_int2Integerzh(i){
	var iobj = _tweak_integer(goog.math.Integer.fromInt(i));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_word2Integerzh(w){
	return __integer_cmm_int2Integerzh(w);
}

function __integer_cmm_andIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var iobj = _tweak_integer(abits.and(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_orIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var iobj = _tweak_integer(abits.or(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_xorIntegerzh(sa, abits, sb, bbits){
	abits = _setsign(sa, abits);
	bbits = _setsign(sb, bbits);
	var iobj = _tweak_integer(abits.xor(bbits));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_mul2ExpIntegerzh(sa, abits, n){
	abits = _setsign(sa, abits);
	var iobj = _tweak_integer(abits.shiftLeft(n));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_fdivQ2ExpIntegerzh(sa, abits, n){
	abits = _setsign(sa, abits);
	var iobj = _tweak_integer(abits.shiftRight(n));
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_complementIntegerzh(sa, abits){
	abits = _setsign(sa, abits);
	var iobj = _tweak_integer(abits.not());
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),iobj);
}

function __integer_cmm_int64ToIntegerzh(a){
	var iobj = _integerFromWord64(a);
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),_tweak_integer(iobj));	
}

function __integer_cmm_word64ToIntegerzh(a){
	var iobj = _integerFromWord64(a);
	return __GHC_Tuple_$28$2C$29(_getsign(iobj),_tweak_integer(iobj));	
}

function __hs_integerToInt64(sa, abits){
        abits = _setsign(sa, abits);
        return _integerToWord64(abits);
}

function __hs_integerToWord64(sa, abits){
        abits = _setsign(sa, abits);
        return _integerToWord64(abits);
}

function ____int_encodeDouble(i,e) {
   return i * Math.pow(2,e);
}

function ____int_encodeFloat(i,e) {
   return i * Math.pow(2,e);
}

function __addbits(arr,bits){
	arr.push(bits >>> 24 & 0xff);
	arr.push(bits >>> 16 & 0xff);	
	arr.push(bits >>> 8 & 0xff);	
	arr.push(bits & 0xff);
}

function __encodeNumber(big,e) {
	var m = Math.pow(2,e);
	if(m === Infinity) {
		switch(_sign(big)) {
			case 1: return Infinity;
			case 0: return 0;
			default: return -Infinity;
		}
	}
	
	var b = [];
	for(var i=big.bits_.length-1;i>=0;i--) __addbits(b,big.getBitsUnsigned(i));
	var l = b.length;
		
	var r = 0;
	for(var i=l-1;i>=1;i--) {
		r += m * Math.pow(2,(l-i-1)*8) * (b[i] & 0xff);
	}

	if(b[0] != 0) {
		r += m * Math.pow(2,(l-1)*8) * b[0];
	}

	return r;
}

function __integer_cbits_encodeDouble(sa,abits,e){
	abits = _setsign(sa, abits);
	return __encodeNumber(abits,e);
}

function __integer_cbits_encodeFloat(sa,abits,e){
	abits = _setsign(sa, abits);
	return __encodeNumber(abits,e);
}
