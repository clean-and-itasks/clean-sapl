
function _not_implemented(name){
	throw "Primitive operation \""+name+"\" is not implemented";
}

// ------------------------------------------------------------------------------
// Just to be sure that the dependent definitions are accesible

function __GHC_Ptr_Ptr(__a1){
    return [0, 'GHC.Ptr.Ptr', __a1];
};

function __GHC_Types_C$23(__a1) {
    return [0, 'GHC.Types.C#', __a1];
};

function __GHC_Types_I$23(__a1) {
    return [0, 'GHC.Types.I#', __a1];
};

var __GHC_Types_$5B$5D = [0, 'GHC.Types.[]'];

function __GHC_Types_$3A(__a1, __a2) {
    return [1, 'GHC.Types.:', __a1, __a2];
};

var __GHC_Tuple_$28$29 = [0,"GHC.Tuple.()"];

function __GHC_Tuple_$28$2C$29(a,b){
	return[0,"GHC.Tuple.(,)",a,b]
};

function __GHC_Tuple_$28$2C$2C$29(a,b,c){
	return[0,"GHC.Tuple.(,,)",a,b,c]
};

function __GHC_Tuple_$28$2C$2C$2C$29(a,b,c,d){
	return[0,"GHC.Tuple.(,,,)",a,b,c,d]
};

function __GHC_Ptr_Ptr(b){
	return[0,"GHC.Ptr.Ptr",b]
} 

function __GHC_Base_returnIO(a,b){
	return __GHC_Tuple_$28$2C$29(b,a)
}

// ------------------------------------------------------------------------------

function _eq(a,b){
	return (a == b);
}

function _ne(a,b){
	return (a != b);
}

function _le(a,b){
	return (a <= b);
}

function _ge(a,b){
	return (a >= b);
}

function _gt(a,b){
	return (a > b);
}

function _lt(a,b){
	return (a < b);
}


