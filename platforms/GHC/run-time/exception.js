function _catch(act, handler, st) {
    try {
        return Sapl.apply(act,[st]);
    } catch(e) {
        return Sapl.apply(handler,[e, st]);
    }
}

function _throw(e){
	throw e;
}