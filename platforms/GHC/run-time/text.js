function print(val){
	var out = document.getElementById("output");
	if(out){
		if(val == "\n"){
			var br = document.createElement("BR");
			out.appendChild(br);
		}else{
			var span = document.createElement("SPAN");
			span.innerHTML = val;
			out.appendChild(span);
		}
	}else{
		console.log(val);
	}
}

function println(val){
	print(val);
	print("\n");
}

// ------------------------------------------------------------------------------
// String and Char handling

function ___unpackAppendCString(s, rest){
	for (var i = s.length - 1; i >= 0; i--) {
		rest = __GHC_Types_$3A(__GHC_Types_C$23(s.charAt(i)), rest);
	}
	return rest;
}

// don't need to be decoded (utf8.decode), because literals UTF8 literals are already encoced by the JS interpreter
function ___unpackCStringUTF8(s){
	return ___unpackAppendCString(s, __GHC_Types_$5B$5D);
}

function ___unpackFoldrCString(str, f, z) {
    var acc = z;
    for(var i = str.length-1; i >= 0; --i) {
        acc = Sapl.feval([f, [__GHC_Types_C$23(str.charAt(i)), acc]]);
    }
    return acc;
}

// TODO: check handle
function ___putChar(handle, chr, world){
	
	chr = Sapl.feval(chr);
	
	// Check if the value is wrapped or not
	if(chr instanceof Array){
		print(chr[2]);
	}else{
		print(chr);
	}
	
	return __GHC_Base_returnIO(__GHC_Tuple_$28$29,world);
}

// TODO: check handle
function ___putStr(handle, str, newline, world){

	str = ___toJSString(Sapl.feval(str));
	if(newline){
		println(str);
	}else{
		print(str);
	}

	return __GHC_Base_returnIO(__GHC_Tuple_$28$29,world);
}

function ___toHSString(ss){
	return ___unpackAppendCString(ss, __GHC_Types_$5B$5D);
}

function ___toJSString(ss){
	var cat = "";
	while (ss[0]!=0) {
		cat += Sapl.feval(ss[2])[2];
		ss = Sapl.feval(ss[3]);
	} 
	return cat;
}
