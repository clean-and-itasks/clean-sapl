// ------------------------------------------------------------------------------
// Arrays and Addr# handling, they are based on each other (mostly borrowed from Haste)
// + malloc, realloc, free ffi

function _newArray(n, x, st) {
    var arr = [];
    for(; n > 0; --n) {
        arr.push(x);
    }
    return __GHC_Tuple_$28$2C$29(st, arr);
}	  

function _writeArray(arr, i, a, st){
	arr[i] = a;
	return st;
}

function _copyArray(a,o1,ma,o2,n,st) {
	for(var i=0;i<n;i++) {
		ma[i+o2] = a[i+o1];
    }
	return st;
}

function _newArrayArray(n, st){
	var arr = []; 
	for(var i=0;i<n;i++) { 
		arr[i] = arr;
	}
    return __GHC_Tuple_$28$2C$29(st, arr);	
}

function _newAlignedByteArray(n, align, st){
	return 	__GHC_Tuple_$28$2C$29(st, __newByteArray(n));
}

function _newByteArray(n, st) {
	return 	__GHC_Tuple_$28$2C$29(st, __newByteArray(n));
}

// Create all views at once; perhaps it's wasteful, but it's better than having
// to check for the right view at each read or write.
// There is no overlapping between 'addr','sptr' and the other views.
function __newByteArray(n) {
    // Pad the thing to multiples of 8.
    var padding = 8 - n % 8;
	var n2 = n;
    if(padding < 8) {
        n2 += padding;
    }
    var arr = {};
    var buffer = new ArrayBuffer(n2);
	var normal = new Array(n);
    var views = {};
    views['i8']  = new Int8Array(buffer);
    views['i16'] = new Int16Array(buffer);
    views['i32'] = new Int32Array(buffer);
    views['w8']  = new Uint8Array(buffer);
    views['w16'] = new Uint16Array(buffer);
    views['w32'] = new Uint32Array(buffer);
    views['f32'] = new Float32Array(buffer);
    views['f64'] = new Float64Array(buffer);
	views['addr'] = normal;
	views['sptr'] = normal;
    // Don't update it directly, only through the views!
	arr['b'] = buffer;
    arr['v'] = views;
	arr['size'] = n;
    // ByteArray and Addr are the same thing, so keep an offset if we get
    // casted.
    arr['off'] = 0;
    
	return arr;
}

function _writeByteArray(type, arr, i, a, st) {
	arr['v'][type][i]=a;
	return st;
}

function _copyByteArray(a,o1,ma,o2,n,st) {
	for(var i=0;i<n;i++) {
		ma.v.w8[i+o2] = a.v.w8[i+o1];
    }
	return st;
}

function _setByteArray(a,o,n,v,st) {
	for(var i=0;i<n;i++) {
		a.v.i8[o+i] = v;
	}
	return st;
}

// ------------------------------------------------------------------------------
// Addr# handling (completely borrowed from Haste)

// An attempt at emulating pointers enough for ByteString and Text to be
// usable without patching the hell out of them.
// The general idea is that Addr# is a byte array with an associated offset.

function __newAddr(addr, off){
    var newaddr = {};
    newaddr['off']  = off;
    newaddr['b']    = addr['b'];
    newaddr['v']    = addr['v'];
    newaddr['size'] = addr['size'];	
	return newaddr;
}

function _plusAddr(addr, off) {
    return __newAddr(addr, addr['off'] + off);
}

function _minusAddr(addr1, addr2) {
    return (addr1['off'] - addr2['off']);
}

function _remAddr(addr, off) {
    return __newAddr(addr, addr['off'] % off);	
}

function _writeOffAddr(type, elemsize, addr, off, x, st) {
	switch(type){
	case 'c': 
		addr['v']['w8'][addr.off/elemsize + off] = x.charCodeAt(0);
		break;
	case 'wc': 
		addr['v']['w32'][addr.off/elemsize + off] = x.charCodeAt(0);
		break;
	default:
		addr['v'][type][addr.off/elemsize + off] = x;
	}
	return st;
}

// Some libraries handle C strings as arrays (e.g. Data.Text)
function _indexCharOffAddr(addr, off) {
	if(typeof addr == "string"){
		if(off < addr.length){
			return addr.charAt(off);
		}else{
			return '\0';
		}
	}else{
		return addr['v']['w8'][addr.off + off];
	}
}

function _indexOffAddr(type, elemsize, addr, off) {
	switch(type){
	case 'c': 
		return String.fromCharCode(addr['v']['w8'][addr.off/elemsize + off]);
	case 'wc': 
		return String.fromCharCode(addr['v']['w32'][addr.off/elemsize + off]);
	default:			
		return addr['v'][type][addr.off/elemsize + off];
	}
}

function _readOffAddr(type, elemsize, addr, off, st) {
	var ret;
	switch(type){
	case 'c': 
		ret = String.fromCharCode(addr['v']['w8'][addr.off/elemsize + off]);
		break;
	case 'wc': 
		ret = String.fromCharCode(addr['v']['w32'][addr.off/elemsize + off]);
		break;
	default:			
		ret = addr['v'][type][addr.off/elemsize + off];
	}
	return __GHC_Tuple_$28$2C$29(st, ret);
}

// Two addresses are equal if they point to the same buffer and have the same
// offset. For other comparisons, just use the offsets - nobody in their right
// mind would check if one pointer is less than another, completely unrelated,
// pointer and then act on that information anyway.
function _eqAddr(a, b) {
    if(a == b) {
        return true;
    }
    return a && b && a['b'] == b['b'] && a['off'] == b['off'];
}

function _ltAddr(a, b) {
    if(a) {
        return b && a['off'] < b['off'];
    } else {
        return (b != 0); 
    }
}

function _gtAddr(a, b) {
    if(b) {
        return a && a['off'] > b['off'];
    } else {
        return (a != 0);
    }
}

// ------------------------------------------------------------------------------
// Memory stuff

var __malloc = __newByteArray;

function __realloc(old, n){
	var arr = _newByteArray(n);
	if(old != null){
		for (i=0; i<Math.min(arr['size'], old['size']); i++){
			arr.v.w8[i] = old.v.w8[i];
		}
	}
	return arr;
}

function __free(n){
	return __GHC_Tuple_$28$29;
}

function __memcpy(target,source,n){
	var toff = target['off'];
	var soff = source['off'];
	for(i=0;i<n;i++){
		target.v.w8[toff+i] = source.v.w8[soff+i];
	}
	return __GHC_Tuple_$28$29;
}

var __memmove = __memcpy

function __memset(target,val,n){
	var toff = target['off'];
	for(i=0;i<n;i++){
		target.v.i8[toff+i] = val;
	}
	return __GHC_Tuple_$28$29;
}

function __memchr(ptr, val, n) {
	var poff = ptr['off'];
	for(var i=0;i<n;i++) {
		if(ptr.v.i8[poff+i] === val) {
			return __newAddr(ptr, poff+1);	
		}
	}
	return null;
}

function __memcmp(ptr1, ptr2, n) {
	for(var i=0;i<n;i++) {
		var a = ptr1.v.w8[ptr1['off']+i];
		var b = ptr2.v.w8[ptr2['off']+i];
		var c = a-b;
		if(c !== 0) { return c; }
	}
	return 0;
}

function __strlen(ptr) {
	var poff = ptr['off'];
	var i=0;
	while(true) {
		if(ptr.v.w8[poff+i] === 0) { return i; }
		i++;
	}
}

// ------------------------------------------------------------------------------
// for bytestrings

function __fps_reverse(target, source, n) {
	for(var i=0;i<n;i++) {
		target.v.w8[target['off']+n-i-1] = source.v.w8[source['off']+i];
	}
}

function __fps_intersperse(target,source,n,c) {
	var toff = target['off'];
	var soff = source['off'];
	for(var i=0;i<n-1;i++) {
		target.v.w8[toff] = source.v.w8[soff+i];
		target.v.w8[toff+1] = c;
		toff += 2;
	}
	if(n > 0) {
		target.v.w8[toff] = source.v.u8[soff+n-1];
	}
}

function __fps_maximum(ptr,n) {
	var poff = ptr['off'];
	var max = ptr.v.w8[poff];
	for(var i=1;i<n;i++) {
		var c = ptr.v.w8[poff+i];
		if(c > max) { max = c; }
	}
	return max;
}

function __fps_minimum(ptr,n) {
	var poff = ptr['off'];
	var min = ptr.v.w8[poff];
	for(var i=1;i<n;i++) {
		var c = ptr.v.w8[poff+i];
		if(c < min) { min = c; }
	}
	return min;
}

function __fps_count(ptr,n,c) {
	var poff = ptr['off'];
	var count = 0;
	for(var i=0;i<n;i++) {
		if(ptr.v.w8[poff+i] === c) { count++; }
	}
	return count|0;
}

/*
foreign import ccall unsafe "static _hs_bytestring_int_dec" c_int_dec
    :: CInt -> Ptr Word8 -> IO (Ptr Word8)

foreign import ccall unsafe "static _hs_bytestring_long_long_int_dec" c_long_long_int_dec
    :: CLLong -> Ptr Word8 -> IO (Ptr Word8)	

foreign import ccall unsafe "static _hs_bytestring_uint_dec" c_uint_dec
    :: CUInt -> Ptr Word8 -> IO (Ptr Word8)

foreign import ccall unsafe "static _hs_bytestring_long_long_uint_dec" c_long_long_uint_dec
    :: CULLong -> Ptr Word8 -> IO (Ptr Word8)	

foreign import ccall unsafe "static _hs_bytestring_uint_hex" c_uint_hex
    :: CUInt -> Ptr Word8 -> IO (Ptr Word8)

foreign import ccall unsafe "static _hs_bytestring_long_long_uint_hex" c_long_long_uint_hex
    :: CULLong -> Ptr Word8 -> IO (Ptr Word8)	
*/