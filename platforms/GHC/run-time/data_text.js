function ___hs_text_memcpy(target,aoff,source,boff,n){
	var toff = target['off'] + (aoff<<1);
	var soff = source['off'] + (boff<<1);
	n = n<<1;
	for(i=0;i<n;i++){
		target.v.w8[toff+i] = source.v.w8[soff+i];
	}
	return __GHC_Tuple_$28$29;
}

function ___hs_text_memcmp(ptr1,aoff,ptr2,boff,n){
	var poff1 = ptr1['off'] + (aoff<<1);
	var poff2 = ptr2['off'] + (boff<<1);
	n = n<<1;
	for(var i=0;i<n;i++) {
		var a = ptr1.v.w8[poff1+i];
		var b = ptr2.v.w8[poff2+i];
		var c = a-b;
		if(c !== 0) { return c; }
	}
	return 0;
}

// From GHCJS
// decoder below adapted from cbits/cbits.c in the text package

/**
 * @define {number} size of Word and Int. If 64 we use goog.math.Long.
 */
var _text_UTF8_ACCEPT = 0;
/**
 * @define {number} size of Word and Int. If 64 we use goog.math.Long.
 */
var _text_UTF8_REJECT = 12

var _text_utf8d =
   [
  /*
   * The first part of the table maps bytes to character classes that
   * to reduce the size of the transition table and create bitmasks.
   */
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
   7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
   8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
  10,3,3,3,3,3,3,3,3,3,3,3,3,4,3,3, 11,6,6,6,5,8,8,8,8,8,8,8,8,8,8,8,

  /*
   * The second part is a transition table that maps a combination of
   * a state of the automaton and a character class to a state.
   */
   0,12,24,36,60,96,84,12,12,12,48,72, 12,12,12,12,12,12,12,12,12,12,12,12,
  12, 0,12,12,12,12,12, 0,12, 0,12,12, 12,24,12,12,12,12,12,24,12,24,12,12,
  12,12,12,12,12,12,12,24,12,12,12,12, 12,24,12,12,12,12,12,12,12,24,12,12,
  12,12,12,12,12,12,12,36,12,36,12,12, 12,36,12,12,12,12,12,36,12,36,12,12,
  12,36,12,12,12,12,12,12,12,12,12,12];

/*
 * A best-effort decoder. Runs until it hits either end of input or
 * the start of an invalid byte sequence.
 *
 * At exit, updates *destoff with the next offset to write to, and
 * returns the next source offset to read from.
 */

// foreign import ccall unsafe "_hs_text_decode_utf8" c_decode_utf8
//    :: MutableByteArray# s -> Ptr CSize
//    -> Ptr Word8 -> Ptr Word8 -> IO (Ptr Word8)
 
function ___hs_text_decode_utf8( dest_v
                               , destoff
                               , src
                               , src_end
                               ) {
  var dsto = destoff.v.w32[destoff['off']];
  var srco = src['off'];
  var state = _text_UTF8_ACCEPT;
  var codepoint;
  var ddv = dest_v.v;
  var sdv = src.v;
  var src_end_o = src_end['offs'];

  function decode(b) {
    var type = _text_utf8d[b];
    codepoint = (state !== _text_UTF8_ACCEPT) ?
      (b & 0x3f) | (codepoint << 6) :
      (0xff >>> type) & b;
    state = _text_utf8d[256 + state + type];
    return state;
  }
  while (srco < src_end_o) {
    if(decode(sdv.w8[srco++]) !== _text_UTF8_ACCEPT) {
      if(state !== _text_UTF8_REJECT) {
        continue;
      } else {
        break;
      }
    }
    if (codepoint <= 0xffff) {
      ddv.w16[dsto] = codepoint;
      dsto += 2;
    } else {
      ddv.w16[dsto] = (0xD7C0 + (codepoint >> 10));
      ddv.w16[dsto+2] = (0xDC00 + (codepoint & 0x3FF));
      dsto += 4;
    }
  }

  /* Error recovery - if we're not in a valid finishing state, back up. */
  if (state != _text_UTF8_ACCEPT)
    srco -= 1;

  destoff.v.w32[destoff['off']] = dsto>>1;
  return __newAddr(src, srco);
}
