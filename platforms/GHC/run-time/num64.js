// ------------------------------------------------------------------------------
// 64 bit number support
// Some of this code is borrowed from ghcjs

function __hs_eqWord64(a,b) {
	return (a.l===b.l && a.h===b.h);
}

function __hs_neWord64(a,b) {
	return (a.l!=b.l || a.h!=b.h);
}

function __hs_leWord64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als < bls || (als === bls && ((a.l&1) <= (b.l&1))));
	} else {
		var ahs = a.h >>> 1;
		var bhs = b.h >>> 1;
		return (ahs < bhs || (ahs === bhs && ((a.h&1) <= (b.h&1))));
	}
}

function __hs_ltWord64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als < bls || (als === bls && ((a.l&1) < (b.l&1))));
	} else {
		var ahs = a.h >>> 1;
		var bhs = b.h >>> 1;
		return (ahs < bhs || (ahs === bhs && ((a.h&1) < (b.h&1))));
	}
}

function __hs_geWord64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als > bls || (als === bls && ((a.l&1) >= (b.l&1))));
	} else {
		var ahs = a.h >>> 1;
		var bhs = b.h >>> 1;
		return (ahs > bhs || (ahs === bhs && ((a.h&1) >= (b.h&1))));
	}
}

function __hs_gtWord64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als > bls || (als === bls && ((a.l&1) > (b.l&1))));
	} else {
		var ahs = a.h >>> 1;
		var bhs = b.h >>> 1;
		return (ahs > bhs || (ahs === bhs && ((a.h&1) > (b.h&1))));
	}
}

function __hs_eqInt64(a,b) {
	return (a.l===b.l && a.h===b.h);
}

function __hs_neInt64(a,b) {
	return (a.l!=b.l || a.h!=b.h);
}

function __hs_leInt64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als < bls || (als === bls && ((a.l&1) <= (b.l&1))));
	} else {
		return (a.h < b.h);
	}
}

function __hs_ltInt64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als < bls || (als === bls && ((a.l&1) < (b.l&1))));
	} else {
		return (a.h < b.h);
	}
}

function __hs_geInt64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als > bls || (als === bls && ((a.l&1) >= (b.l&1))));
	} else {
		return (a.h > b.h);
	}
}

function __hs_gtInt64(a,b) {
	if(a.h === b.h) {
		var als = a.l >>> 1;
		var bls = b.l >>> 1;
		return (als > bls || (als === bls && ((a.l&1) > (b.l&1))));
	} else {
		return (a.h > b.h);
	}
}

function __hs_quotInt64(a,b) {
	var c = goog.math.Long.fromBits(a.l,a.h).div(goog.math.Long.fromBits(b.l,b.h));
	return {l: c.getLowBits(), h: c.getHighBits()};
}

function __hs_remInt64(a,b) {
	var c = goog.math.Long.fromBits(a.l,a.h).modulo(goog.math.Long.fromBits(b.l,b.h));
	return {l: c.getLowBits(), h: c.getHighBits()};
}

function __hs_plusInt64(a,b) {
	var c = goog.math.Long.fromBits(a.l,a.h).add(goog.math.Long.fromBits(b.l,b.h));
	return {l: c.getLowBits(), h: c.getHighBits()};
}

function __hs_minusInt64(a,b) {
	var c = goog.math.Long.fromBits(a.l,a.h).subtract(goog.math.Long.fromBits(b.l,b.h));
	return {l: c.getLowBits(), h: c.getHighBits()};
}

function __hs_timesInt64(a,b) {
	var c = goog.math.Long.fromBits(a.l,a.h).multiply(goog.math.Long.fromBits(b.l,b.h));
	return {l: c.getLowBits(), h: c.getHighBits()};
}

function __hs_negateInt64(a) {
	var c = goog.math.Long.fromBits(a.l,a.h).negate();
	return {l: c.getLowBits(), h: c.getHighBits()};
}

function __hs_quotWord64(a,b) {
	var a = _integerFromWord64(a);
	var b = _integerFromWord64(b);  
	var c = a.divide(b);
	return {l: c.toInt(), h: c.shiftRight(32).toInt()};
}

function __hs_remWord64(a,b) {
	var a = _integerFromWord64(a);
	var b = _integerFromWord64(b);  
	var c = a.modulo(b);
	return {l: c.toInt(), h: c.shiftRight(32).toInt()};
}

function __hs_not64(a) {
	return {l: ~a.l, h: ~a.h};
}

function __hs_xor64(a,b) {
	return {l: a.l ^ b.l, h: a.h ^ b.h};
}

function __hs_and64(a,b) {
	return {l: a.l & b.l, h: a.h & b.h};
}

function __hs_or64(a,b) {
	return {l: a.l | b.l, h: a.h | b.h};
}

function __hs_uncheckedIShiftL64(a,n) {
	var num = new goog.math.Long(a.l,a.h).shiftLeft(n);
	return {l: num.getLowBits(), h: num.getHighBits()};
}

function __hs_uncheckedIShiftRA64(a,n) {
	var num = new goog.math.Long(a.l,a.h).shiftRight(n);
	return {l: num.getLowBits(), h: num.getHighBits()};
}

// always nonnegative n?
function __hs_uncheckedShiftL64(a,n) {
	n &= 63;
	if(n == 0) {
		return a;
	} else if(n < 32) {
		return {l: a.l << n, h: (a.h << n) | (a.l >>> (32-n))};
	} else {
		return {l: 0, h: ((a.l << (n-32))|0)};
	}
}

function __hs_uncheckedShiftRL64(a,n) {
	n &= 63;
	if(n == 0) {
		return a;
	} else if(n < 32) {
		return {l: (a.l >>> n ) | (a.h << (32-n)), h: a.h >>> n};
	} else {
		return {l: a.h >>> (n-32), h: 0};
	}
}

// this does an unsigned shift, is that ok?
function __hs_uncheckedShiftRL64(a,n) {
	if(n < 0) throw "unexpected right shift";
	n &= 63;
	if(n == 0) {
		return a;
	} else if(n < 32) {
		return {l: (a.h >>> n) | (a.l << (32 - n)), h: (a.l >>> n)};
	} else {
		return {l: a.h >>> (n - 32), h: 0};
	}
}

function __hs_int64ToWord64(a) {
	return a;
}

function __hs_word64ToInt64(a) {
	return a;
}

function __hs_intToInt64(a) {
	return {l: a, h: (a < 0) ? -1 : 0};	
}

function __hs_int64ToInt(a) {
  return (a.h < 0) ? (a.l | 0x80000000) : (a.l & 0x7FFFFFFF);
}

function __hs_word64ToWord(a) {
  return a.l;
}

function __hs_wordToWord64(w) {
  return {l: w, h: 0};
}

