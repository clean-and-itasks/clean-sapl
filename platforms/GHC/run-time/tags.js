function _tagToEnum(a){
	return [a];
}

function _tagToBool(a){
	return (a==0?false:true);
}

function _dataToTag(a){
	if(typeof a == "boolean"){
		return (a?1:0);
	}else{
		return a[0];
	}
}
