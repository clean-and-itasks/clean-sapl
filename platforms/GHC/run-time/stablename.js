// Implementation of stable names. From Haste.
// Unlike native GHC, the garbage collector isn't going to move data around
// in a way that we can detect, so each object could serve as its own stable
// name if it weren't for the fact we can't turn a JS reference into an
// integer.
// So instead, each object has a unique integer attached to it, which serves
// as its stable name.

var __next_stable_name = 1;

function _makeStableName(x,st) {
    if(!x.stableName) {
        x.stableName = __next_stable_name;
        __next_stable_name += 1;
    }
    return __GHC_Tuple_$28$2C$29(st, x.stableName);
}

function _eqStableName(x, y) {
    return (x == y) ? 1 : 0;
}

function _stableNameToInt(x) {
	return x;
}