// based on the ghcjs version

function __MD5Init(ctx) {
	ctx.crypt = new goog.crypt.Md5();
	return __GHC_Tuple_$28$29;
}

function __MD5Update(ctx, data, len) {
	var arr = new Uint8Array(data.b, data.off);
	ctx.crypt.update(arr, len);
	return __GHC_Tuple_$28$29;
}

function __MD5Final(dst, ctx) {
	var digest = ctx.crypt.digest();
	for(var i=0;i<16;i++) {
		dst.v.w8[dst.off+i] = digest[i];
	}
	return __GHC_Tuple_$28$29;
}
