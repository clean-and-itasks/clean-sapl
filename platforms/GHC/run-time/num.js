// ------------------------------------------------------------------------------
// Some supplementary numeric functions
// Some codes are borrowed from Haste

function _mulIntMayOflo(x,y){
	var tmp = x*y; 
	return ((tmp===(tmp|0))?0:1);
}

function _addIntC(a, b) {
    var x = a+b;
    return __GHC_Tuple_$28$2C$29(x & 0xffffffff, x > 0x7fffffff);
}

function _subIntC(a, b) {
    var x = a-b;
    return __GHC_Tuple_$28$2C$29(x & 0xffffffff, x < -2147483648);
}

function _timesWord(a, b) {
  // ignore high a * high a as the result will always be truncated
  var lows = (a & 0xffff) * (b & 0xffff); // low a * low b
  var aB = (a & 0xffff) * (b & 0xffff0000); // low a * high b
  var bA = (a & 0xffff0000) * (b & 0xffff); // low b * high a
  return (lows + aB + bA) | 0; // sum will not exceed 52 bits, so it's safe
}

function _numberOfSetBits(i){
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

function _sinh(arg) {
    return (Math.exp(arg) - Math.exp(-arg)) / 2;
}

function _tanh(arg) {
    return (Math.exp(arg) - Math.exp(-arg)) / (Math.exp(arg) + Math.exp(-arg));
}

function _cosh(arg) {
    return (Math.exp(arg) + Math.exp(-arg)) / 2;
}

// Scratch space for byte arrays.
var rts_scratchBuf = new ArrayBuffer(8);
var rts_scratchW32 = new Uint32Array(rts_scratchBuf);
var rts_scratchFloat = new Float32Array(rts_scratchBuf);
var rts_scratchDouble = new Float64Array(rts_scratchBuf);

function _decodeDouble_2Int(x) {
    rts_scratchDouble[0] = x;
    var sign = x < 0 ? -1 : 1;
    var manHigh = rts_scratchW32[1] & 0xfffff;
    var manLow = rts_scratchW32[0];
    var exp = ((rts_scratchW32[1] >> 20) & 0x7ff) - 1075;
    if(exp === 0) {
        ++exp;
    } else {
        manHigh |= (1 << 20);
    }
    return __GHC_Tuple_$28$2C$2C$2C$29(sign, manHigh, manLow, exp);
}

function _decodeFloat_Int(x) {
    rts_scratchFloat[0] = x;
    var sign = x < 0 ? -1 : 1;
    var exp = ((rts_scratchW32[0] >> 23) & 0xff) - 150;
    var man = rts_scratchW32[0] & 0x7fffff;
    if(exp === 0) {
        ++exp;
    } else {
        man |= (1 << 23);
    }
    return __GHC_Tuple_$28$2C$29(sign*man, exp);
}

function __isDoubleNegativeZero(d) {
  return (d===0 && (1/d) === -Infinity) ? 1 : 0;
}

function __isFloatNegativeZero(d) {
  return (d===0 && (1/d) === -Infinity) ? 1 : 0;
}

function __isDoubleInfinite(d) {
  return (d === Number.NEGATIVE_INFINITY || d === Number.POSITIVE_INFINITY) ? 1 : 0;
}

function __isFloatInfinite(d) {
  return (d === Number.NEGATIVE_INFINITY || d === Number.POSITIVE_INFINITY) ? 1 : 0;
}

function __isFloatFinite(d) {
  return (d !== Number.NEGATIVE_INFINITY && d !== Number.POSITIVE_INFINITY && !isNaN(d)) ? 1 : 0;
}

function __isDoubleFinite(d) {
  return (d !== Number.NEGATIVE_INFINITY && d !== Number.POSITIVE_INFINITY && !isNaN(d)) ? 1 : 0;
}

function __isDoubleNaN(d) {
  return (isNaN(d)) ? 1 : 0;
}

function __isFloatNaN(d) {
  return (isNaN(d)) ? 1 : 0;
}

function __isDoubleDenormalized(d) {
  return (d !== 0 && Math.abs(d) < 2.2250738585072014e-308) ? 1 : 0;
}

function __isFloatDenormalized(d) {
  return (d !== 0 && Math.abs(d) < 2.2250738585072014e-308) ? 1 : 0;
}

function __rintFloat(val){
  return Math.round(val); 
}
