// ------------------------------------------------------------------------------
// FFI handling

function ___ffi_wrapper_gen(args){
	args = Array.prototype.slice.call(args, 0);
	var func = args.shift();
	var world = args.pop();
		
	var r = func.apply(null,args);
	
	// workaround for GHC problem with marshalling/unmarshalling booleans.
	if(typeof r === "boolean") r = r?1:0;
	
	return __GHC_Base_returnIO(r, world);	
}

function _ffi_wrapper2(func,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper3(func,a,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper4(func,a,b,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper5(func,a,b,c,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper6(func,a,b,c,d,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper7(func,a,b,c,d,e,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper8(func,a,b,c,d,e,f,world) { 
	return ___ffi_wrapper_gen(arguments);
}

function _ffi_wrapper9(func,a,b,c,d,e,f,g,world) { 
	return ___ffi_wrapper_gen(arguments);
}
