// MVar implementation. From Haste.
// Since Haste isn't concurrent, takeMVar and putMVar don't block on empty
// and full MVars respectively, but terminate the program since they would
// otherwise be blocking forever.

function _newMVar(st) {
	return __GHC_Tuple_$28$2C$29(st, {empty: true});
}

function _tryTakeMVar(mv, st) {
	if(mv.empty) {
        return __GHC_Tuple_$28$2C$2C$29(st, 0, undefined);
    } else {
        var val = mv.x;
        mv.empty = true;
        mv.x = null;
		return __GHC_Tuple_$28$2C$2C$29(st, 1, val);
    }
}

function _takeMVar(mv,st) {
    if(mv.empty) {
        // TODO: real BlockedOnDeadMVar exception, perhaps?
        err("Attempted to take empty MVar!");
    }
    var val = mv.x;
    mv.empty = true;
    mv.x = null;
	return __GHC_Tuple_$28$2C$29(st, val);
}

function _putMVar(mv,val,st) {
    if(!mv.empty) {
        // TODO: real BlockedOnDeadMVar exception, perhaps?
        err("Attempted to put full MVar!");
    }
    mv.empty = false;
    mv.x = val;
	return st;
}

function _tryPutMVar(mv,val,st) {
    if(!mv.empty) {
        return __GHC_Tuple_$28$2C$29(st, 0);
    } else {
        mv.empty = false;
        mv.x = val;
        return __GHC_Tuple_$28$2C$29(st, 1);
    }
}

function _sameMVar(a, b) {
    return (a == b);
}

function _isEmptyMVar(mv,st) {
	return __GHC_Tuple_$28$2C$29(st, mv.empty ? 1 : 0);
}
