

function __hashable_fnv_hash_offset(addr,len,offset,hash) {
	var toff = addr['off'] + offset;
	for (i = len; i > 0; i-- ) {
		var v = addr.v.i8[toff+i];
		hash = ((hash * 16777619) ^ v) & 0xFFFFFFFF;
	}
	return hash & 0xFFFFFFFF;
}

function __hashable_fnv_hash(addr,len,hash) {
	__hashable_fnv_hash_offset(addr,len,0,hash);
}

function __rts_getThreadId (a) { 
	alert("rts_getThreadId: not implemented");
}