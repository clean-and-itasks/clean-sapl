module Main where

import Text.ParserCombinators.Parsec
import Control.Monad (when)
import System.Environment
import System.Console.GetOpt

import Parser
import Printer

import System.FilePath ((</>), takeDirectory)
import System.Directory (createDirectoryIfMissing)

import Data.Char (isAlphaNum)

-- usage info
usageHeader :: String
usageHeader = "Usage: ghcsapl-ffi <file in> <file out>"

-- Command line options
data Options = Options {
  convertFile :: IO String,
  inFileName  :: Maybe String,
  hsFileName  :: Maybe String,
  jsFileName  :: Maybe String
}

defaultOptions :: Options
defaultOptions = Options (return "[]") Nothing Nothing Nothing

options :: [OptDescr (Options -> Options)]
options = [
  Option ['c'] ["convert"] (ReqArg (\s o -> o { convertFile = readFile s}) "FILE") "file with type definitinitions to convert",
  Option ['i'] ["in"]      (ReqArg (\s o -> o { inFileName = Just s})      "FILE") "input file with ffi definition",
  Option ['o'] ["out"]     (ReqArg (\s o -> o { hsFileName = Just s})      "FILE") "output haskell file",
  Option ['j'] ["js-file"] (ReqArg (\s o -> o { jsFileName = Just s})      "FILE") "javascript file to output"
  ]


debug :: Bool
debug = False

main :: IO()
main = do
  args <- getArgs
  case args of
    [origfile, infile, outfile] -> pn args >> doParse' origfile infile outfile
    [infile, outfile] -> pn args >> doParse' infile infile outfile
    _ -> putStrLn $ usageHeader ++ ("\n found:" ++ show args)

  where
    pn = putStrLn . ((++) "preprocessing ffi:") . show 


doParse' :: FilePath -> FilePath -> FilePath -> IO ()
doParse' orgFile infile outfile = do
  doParse [] orgFile infile outfile

doParse :: ConvertMap -> String -> String ->String -> IO ()
doParse cData _orgFile inFile outFile = do
  contents <- readFile inFile
  --putStrLn . show $ contents
  let ls = parse (parseFFIFile cData) inFile contents
  --putStrLn . show $ ls
  case ls of
    Right l  -> do 
      --putStrLn $ ("orgFile:" ++ orgFile)
      --putStrLn $ ("inFile:" ++ inFile)
      --putStrLn $ ("outFile:" ++ outFile)
      --putStrLn $ (">>>" ++ ("xxx" </> path </> "yyy"))
      --let l' = map (mapFFILine (\str -> fmap (\c -> if isAlphaNum c then c else '_')(path </> str))) l
      --putStrLn ("path:" ++ show (path,path'))
      --putStrLn . show . filter isFFILine $ l
      ----putStrLn path'
      let modlist = map modname . filter isModLine $ l
      if (null modlist)
        then putStrLn $ "Error: no module name in hs source file"
        else do
          let mod = ("." ++ head modlist)
          let ffiNameUS = fmap (\c -> if isAlphaNum c then c else '_') (head modlist)
          let l' = map (mapFFILine (\str ->  ('_':ffiNameUS) ++ fmap (\c -> if isAlphaNum c then c else '_') str)) l
          let ffiPathSL = fmap (\c -> if c == '.' then '/' else c) (head modlist)
          let jsFile = "./ffis/" ++ ffiPathSL ++ ".ffipp.js"
          writeFilesOut outFile jsFile l'
    Left p -> putStrLn $ "Error: " ++ (show p)
  where
    --isFFILine (PlainLine _) = False
    --isFFILine (FFILine _ _ _ _ _) = True
    --isFFILine (ModLine _ _) = False
    isModLine (ModLine _ _) = True
    isModLine _ = False

writeFilesOut :: String -> String -> [FFILine] -> IO ()
writeFilesOut hsFile jsFile ls = do
  when debug $ putStrLn $ show ls
  let hsData = haskellFile ls
      jsData = javascriptFile ls
  --putStrLn . ((++) "ffi-preprocessor writing hs file: ") $ hsFile
  writeFile hsFile hsData
  when (not . null $ jsData) $ do
    --putStrLn . ((++) "ghcsapl-ffi writing js file:") $ jsFile
    createDirectoryIfMissing True (takeDirectory jsFile)
    --putStrLn $ "jsData:" ++ jsData
    writeFile jsFile jsData

mapFFILine :: (String -> String) -> FFILine -> FFILine
mapFFILine _ p@(PlainLine _) = p
mapFFILine f l@(FFILine _ _ _ _ _) = l { hsForeignName = Just . f . hsName $ l }
mapFFILine _ p@(ModLine _ _) = p

--uniqueName :: FFILine -> IO FFILine
--uniqueName p@(PlainLine _) = return p
--uniqueName l@(FFILine _ _ _ _ _) = do
--  --putStrLn . show $ line
--  rds <- mapM (const (randomRIO (0,61))) $ take 8 ([1..]::[Int]) :: IO [Int]
--  return $ l { hsForeignName = Just (hsName l ++ (map toC $ rds)) }
--  where
--    toC :: Int -> Char
--    toC n | n < 10 = toEnum (n + fromEnum '0') 
--          | n >= 10 && n < 36 = toEnum (n - 10 + fromEnum 'a') 
--          | n >= 36 && n < 62 = toEnum (n - 36 + fromEnum 'A') 
--          | otherwise = 'A'



