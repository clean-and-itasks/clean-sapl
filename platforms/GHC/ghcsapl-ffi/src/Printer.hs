
module Printer where

import qualified Parser as P
import Data.Maybe (fromMaybe)

-- helper function to join strings with separator
joinStrings :: Char -> [String] -> String
joinStrings _ [] = []
joinStrings _ (s:[]) = s
joinStrings c (s:ss) = s ++ [c] ++ (joinStrings c ss)

-- convert the haskellname to the name of the foreign function
toForeignName :: String -> String
toForeignName = (++ "_CImpl")

haskellFile :: [P.FFILine] -> String
haskellFile = concat . map haskellLine

haskellLine :: P.FFILine -> String
haskellLine (P.PlainLine s) = s ++ "\n"
haskellLine (P.ModLine _ s) = s ++ "\n"

haskellLine (P.FFILine _ hsName hsForeignName _cConstr hsType) =
  if needsConversion hsType then
    --expression with something that needs conversion
    -- so output to lines, the foreign call with converted types, and the conversion function
    "foreign import ccall \"" ++ fname ++ "\" " ++ hsName ++ "_With_CTypes :: " ++ (foreignSignature hsType) ++ "\n"
    ++ hsName ++ argumentList ++ "= " ++ resultConversion ++ hsName ++ "_With_CTypes " ++ argumentListWithConversion ++ "\n"
  else
    --expression without strings
    "foreign import ccall \"" ++ fname ++ "\" " ++ hsName ++ " :: " ++ (foreignSignature hsType) ++ "\n"
  where
    fname = fromMaybe (toForeignName hsName) hsForeignName
    -- return true if there is a IO or function type in the argument list
    -- return true if there is coversion type with to conversion in argument list
    -- return true of there is converion type with from conversion as result
    needsConversion t = case t of
      (P.IOType t')        -> needsConversion t'
      (P.ConvertType dat)  -> if null (P.fromConvert dat) then False else True
      (P.FunctionType a r) -> argNeedsConversion a || needsConversion r
      _                  -> False
    argNeedsConversion t = case t of
      (P.ConvertType dat)  -> if null (P.toConvert dat) then False else True
      P.IOVoid             -> True
      (P.IOType _)         -> True
      (P.FunctionType _ _) -> True
      _                  -> False
    numArgs = numArgs' hsType
      where
      numArgs' (P.FunctionType _ r) = 1 + (numArgs' r)
      numArgs' _                  = 0
    argumentList :: String
    argumentList = concatMap (\i -> " a" ++ (show i)) $ ([1..numArgs] :: [Int])
    argumentListWithConversion :: String
    argumentListWithConversion = joinStrings ' ' $ zipWith argumentConversion ([1..] :: [Int]) (argTypes hsType)
      where
      argumentConversion i t = case t of
        (P.ConvertType dat)  -> if null (P.toConvert dat) then
                                 "a" ++ (show i)
                              else
                                 "(" ++ (P.toConvert dat) ++ " a" ++ (show i) ++")"
        (P.FunctionType _ _) -> "(mkCallback $ a" ++ (show i) ++ ")"
        P.IOVoid             -> "(mkCallback $ a" ++ (show i) ++ ")"
        P.IOType _           -> "(mkCallback $ a" ++ (show i) ++ ")"
        _                  -> "a" ++ (show i)
    resultConversion = resultConversion' (resultType hsType)
      where
      resultConversion' t = case t of
        P.ConvertType dat -> if null (P.fromConvert dat) then "" else (P.fromConvert dat) ++ " $ "
        _               -> ""
      
  
    argTypeList :: P.Type -> String
    argTypeList t = concatMap (\a-> showArgType a ++ " -> ") $ argTypes t
    foreignSignature :: P.Type -> String
    {-cConstraintString classConstr = (className classConstr) ++ concatMap (\p -> ' ':p) (parameters classConstr)
    cConstraintsString
      | cConstr == [] = ""
      | otherwise     = "(" ++ joinStrings ',' (map cConstraintString cConstr) ++ ") => "-}
    foreignSignature t = {-cConstraintsString ++-} (argTypeList t) ++ (showResType . resultType $ t)
    showArgType :: P.Type -> String
    showArgType (P.ConvertType dat)   = P.foreignTypeName dat
    showArgType P.IOVoid              = "JSFun (IO ())"
    showArgType (P.IOType t)          = "JSFun (IO (" ++ showArgType t ++ "))"
    showArgType (P.PlainType s)       = s
    showArgType (P.FunctionType f r)  = "JSFun (" ++ foreignSignature (P.FunctionType f r) ++ ")"
    showResType :: P.Type -> String
    showResType (P.ConvertType dat)   = P.foreignTypeName dat
    showResType P.IOVoid              = "IO ()"
    showResType (P.IOType t)          = "IO (" ++ showResType t ++ ")"
    showResType (P.PlainType s)       = s
    showResType (P.FunctionType f r)  = "(" ++ foreignSignature (P.FunctionType f r) ++ ")"


javascriptFile :: [P.FFILine] -> String
javascriptFile = concat . map javascriptLine

javascriptLine :: P.FFILine -> [Char]
javascriptLine (P.PlainLine _) = ""
javascriptLine (P.ModLine _ _) = ""
javascriptLine (P.FFILine jsExp hsName hsForeignName _cConstr hsType) =
  "function " ++ fname ++ "(" ++ (argumentList $ length (argTypes hsType)) ++ {-ioArg ++-} ") {\n  "
  ++ if (resultType hsType) == P.IOVoid then jsCommand ++ ";\n  return;\n}\n" else "  return " ++ jsCommand ++ ";\n}\n"
  where
    fname = "__" ++ fromMaybe (toForeignName hsName) hsForeignName
    argumentList :: Int -> String
    argumentList maxv = concatWith "," . map (\i -> "a" ++ (show i)) $ [1..maxv]
    concatWith :: String -> [String] -> String
    concatWith sep (x:y:xs) = x ++ sep ++ (concatWith sep (y:xs))
    concatWith _ (x:[])     = x
    concatWith _  _ = ""
    --ioArg = if null (argTypes hsType) then "_" else  ",_"
    jsCommand = concat . map showExprPart $ jsExp
    showExprPart :: P.JSExprPart -> String
    showExprPart (P.StringPart s) = s
    showExprPart (P.ArgumentPart i) = "a" ++ (show i)
    showExprPart (P.RestArgPart) = concatWith "," . map showExprPart $ restArguments
    restArguments :: [P.JSExprPart]
    restArguments = let argId (P.ArgumentPart i) = i
                        argId _ = 0
                        highestArgument = maximum . map (argId) $ jsExp
                        numArguments = length (argTypes hsType)
                        missingArgs = if highestArgument >= numArguments then [] else [(highestArgument+1) .. numArguments]
                    in map (\i -> P.ArgumentPart i) missingArgs

-- helper functions
argTypes :: P.Type -> [P.Type]
argTypes t = case t of 
  P.FunctionType a r -> a:(argTypes r)
  _                -> []
  
resultType :: P.Type -> P.Type
resultType t = case t of
  P.FunctionType _ r -> resultType r
  r                -> r
