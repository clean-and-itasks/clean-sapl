#!/bin/bash
HOME="/home/vagrant"
cd $HOME
cabal update

/bin/bash /vagrant/build/build-sub-ghc.sh

svn co https://svn.cs.ru.nl/repos/clean-sapl/trunk/ /tmp/sapl-src/

/bin/bash /vagrant/build/build-sub-sapl.sh
/bin/bash /vagrant/build/build-sub-ghcsapl.sh
/bin/bash /vagrant/build/build-sub-cc.sh

chown -R vagrant:vagrant $HOME/.cabal
chown -R vagrant:vagrant $HOME/sapl

# create prebuilt package
tar -cf sapl.tar bin sapl
bzip2 sapl.tar

mkdir -p $HOME/sapl/packages
mkdir -p $HOME/sapl/run-time

cp -r /tmp/sapl-src/platforms/GHC/run-time/* $HOME/sapl/run-time/
cp -r /tmp/sapl-src/platforms/Common/run-time/* $HOME/sapl/run-time/
cp -r /tmp/sapl-src/platforms/GHC/packages/7.6.3-32/* $HOME/sapl/packages/
chown -R vagrant:vagrant $HOME/sapl/

rm -rf /tmp/sapl-src/

/bin/bash /vagrant/build/build-sub-java.sh

# add binaries to the path
cat /vagrant/config/profile >> .profile

