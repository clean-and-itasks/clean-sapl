#!/bin/bash

# install the Clean compiler
wget http://clean.cs.ru.nl/download/Clean24/linux/clean2.4.tar.gz
tar -xzvf clean2.4.tar.gz
cd clean
make
cd $HOME
rm -f clean2.4.tar.gz
PATH="$HOME/clean/bin:$PATH"

# build and install SAPL tools

cd /tmp/sapl-src/deps/Platform/OS-Independent/Text/Unicode
make -f Makefile32
cd $HOME

mkdir -p $HOME/sapl/bin
mkdir -p $HOME/sapl/share

cd /tmp/sapl-src/src
chmod +x make.linux32.sh
./make.linux32.sh

cp sl sapl2js $HOME/sapl/bin
cp ghc.f clean.f $HOME/sapl/share
cd $HOME

rm -rf clean
