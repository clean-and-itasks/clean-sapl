#!/bin/bash
cd /home/vagrant
wget http://www.haskell.org/ghc/dist/7.6.3/ghc-7.6.3-src.tar.bz2
tar xjvf ghc-7.6.3-src.tar.bz2
cp /vagrant/config/build.mk ghc-7.6.3/mk/
cd ghc-7.6.3
./configure --prefix=/home/vagrant/ghc
make -j 8
make install

# create prebuilt package
tar -cf ghc.tar ghc
bzip2 ghc.tar

