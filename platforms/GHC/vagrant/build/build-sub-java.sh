#!/bin/bash

# install JAVA 7
# http://stackoverflow.com/questions/10268583/how-to-automate-download-and-installation-of-java-jdk-on-linux
wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u67-b01/jre-7u67-linux-i586.tar.gz"
mkdir -p /usr/lib/jre
mv jre-7u67-linux-i586.tar.gz /usr/lib/jre
cd /usr/lib/jre
tar zxvf jre-7u67-linux-i586.tar.gz
rm jre-7u67-linux-i586.tar.gz

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jre/jre1.7.0_67/bin/java" 1
sudo update-alternatives --set "java" "/usr/lib/jre/jre1.7.0_67/bin/java"

cd $HOME
