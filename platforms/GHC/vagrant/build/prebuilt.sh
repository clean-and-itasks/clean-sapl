#!/bin/bash
HOME="/home/vagrant"
cd $HOME
cabal update

# install ghc 7.6.3
wget http://people.inf.elte.hu/dlacko/sapl-prebuilt/ghc.tar.bz2
bzip2 -d ghc.tar.bz2
tar -xvf ghc.tar
rm -f ghc.tar

# install other tools
#wget http://people.inf.elte.hu/dlacko/sapl-prebuilt/sapl.tar.bz2
wget https://drive.google.com/uc?export=download\&confirm=\&id=0B_CuJrgghKfyQ1hWTG81MUlZQUE -O sapl.tar.bz2
bzip2 -d sapl.tar.bz2
tar -xvf sapl.tar
rm -f sapl.tar

svn co https://svn.cs.ru.nl/repos/clean-sapl/trunk/ /tmp/sapl-src/

mkdir -p $HOME/sapl/packages
mkdir -p $HOME/sapl/run-time

cp -r /tmp/sapl-src/platforms/GHC/run-time/* $HOME/sapl/run-time/
cp -r /tmp/sapl-src/platforms/Common/run-time/* $HOME/sapl/run-time/
cp -r /tmp/sapl-src/platforms/GHC/packages/7.6.3-32/* $HOME/sapl/packages/
chown -R vagrant:vagrant $HOME/sapl/

rm -rf /tmp/sapl-src/

/bin/bash /vagrant/build/build-sub-java.sh

# add binaries to the path
cat /vagrant/config/profile >> .profile

