#!/bin/bash

mkdir -p $HOME/sapl/bin

cd /tmp/sapl-src/platforms/GHC/ghcsapl
cabal install
mv $HOME/.cabal/bin/ghcsapl $HOME/sapl/bin

cd /tmp/sapl-src/platforms/GHC/ghcsapl-ffi
cabal install
mv $HOME/.cabal/bin/ghcsapl-ffi $HOME/sapl/bin

cd $HOME
