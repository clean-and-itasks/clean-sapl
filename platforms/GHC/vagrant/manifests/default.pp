exec { "apt-update":
    command => "apt-get update",
    path => "/usr/bin",	
}

package { 'ghc': ensure => present }
package { 'cabal-install': ensure => present }
package { 'make': ensure => present }
package { 'libncurses5-dev': ensure => present }
package { 'subversion': ensure => present }

Exec["apt-update"] -> Package <| |>

