{-# LANGUAGE CPP #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE NoMonomorphismRestriction, TupleSections, OverloadedStrings, StandaloneDeriving, DeriveFunctor #-}
{-# LANGUAGE ImpredicativeTypes #-}
   
module Compiler.StgToSapl (
    stgToSapl, Ty, ExtTy, Ret(..)
    )
    where

import TyCon (TyCon)
import Module (ModuleName)
import Var (Id,varName, varUnique, isLocalVar)
import Outputable (Outputable)
import StgSyn -- (StgBinding)
import DataCon (DataCon, dataConName, dataConTyCon)
import Literal (Literal(..))
import CoreSyn (AltCon(..))
import TcType (isBoolTy)

import Control.Arrow (second, first)

import qualified Compiler.SAPLStruct as S
import Data.Monoid (mempty, mconcat, Monoid)

import qualified Data.Map as Map
import qualified Data.Set as Set
import Compiler.Predefined (predefined)
import qualified Type as Var (Var)

import qualified Data.List as L
--import Data.List.Utils -- MissingH package
import Data.Bits

import Compiler.Utils (showOutputable)

--type StgBindingIds = (StgBinding,[(Id,[Id])])

type Ty = Maybe TyCon
type RetTy = Maybe TyCon    -- return type (type of the expression)
type ExtTy = [DataCon]     -- external TyCon
type FixTy = Maybe String    -- local TyCon (need to be fixed)

data Ret a = Ret { extTy::ExtTy, retTy::RetTy, fixTy::FixTy, func::a }

type FixVar = String -> S.Var
	    
stgToSapl :: ModuleName -> [String] -> [StgBinding] ->  [Ret S.Func]
stgToSapl modName fixTys = concatMap (stgToSapl') 
    where
        stgToSapl' (StgNonRec bndr stgRhs) = [stgRhsToSapl fixVar bndr stgRhs]  
        stgToSapl' (StgRec stgRhss) = map (\(i,stg) -> stgRhsToSapl fixVar i stg) stgRhss -- [(bndr, GenStgRhs bndr occ)]  
        fixVar = S.Var . fixPredef fix
        fix s = if Set.member s fixSet then modName' ++ s else s  
        fixSet = Set.fromList fixTys
        fixPredef f s = maybe (f s) id $ Map.lookup s predefined
        modName' = showOutputable modName  ++ "."
		
stgRhsToSapl :: FixVar -> Id -> StgRhs -> Ret S.Func -- (ExtTy,(Ty,S.Func))
stgRhsToSapl fixVar n (StgRhsClosure _ccs _sbi _occs _uf _srt bnrs stgExpr) = 
    Ret extTy' ty (exclQual . showOutputable $ n) (if null bnrs then caf else func)
    where 
        func = S.FTFunc (S.FuncName (fixGlobalVar fixVar n) 0) (map (fixGlobalVar fixVar) bnrs) expr
        caf = S.FTCAF (S.FuncName (fixGlobalVar fixVar n) 0) expr
        (extTy',(ty,expr)) = stgExprToSapl fixVar stgExpr

stgRhsToSapl fixVar n (StgRhsCon _ccs  dataCon args) = dataConToSapl fixVar n dataCon args

exclQual :: String -> Maybe String
exclQual n = if '.' `elem` n then Nothing else Just n 
deriving instance Functor GenStgArg
	
dataConToSapl :: FixVar -> Var.Var -> DataCon -> [GenStgArg Id] -> Ret S.Func
dataConToSapl fixVar n dataCon args = if eq then ret adt else ret caf
   where
       eq = (showOutputable . varName $ n) == (showOutputable . dataConName $ dataCon)
       ret = Ret mempty (Just ty) (exclQual . showOutputable  $ n)
       adt = S.FTADT (fixGlobalVar fixVar n) [(appName, length args)]
       caf = S.FTCAF (S.FuncName (n') 0) (mkEApp appName args')
       ty = dataConTyCon dataCon
       appName = fixGlobalDataCon fixVar . dataConName $ dataCon
       args' = map (stgArgToSapl fixVar) args
       n' = fixGlobalVar fixVar n
	   
type RetExpr a = (ExtTy,(Ty,a))
retExpr:: a -> b -> c -> (a,(b,c))
retExpr a b c = (a,(b,c)) 
emptyRE :: Monoid mon => a -> (mon, (Maybe b, a))
emptyRE = retExpr mempty Nothing

stgExprToSapl :: FixVar -> GenStgExpr Id Id -> RetExpr S.Expr
stgExprToSapl fixVar (StgApp occ args) = emptyRE $ mkEApp (fixGlobalVar fixVar occ) (map (stgArgToSapl fixVar) args)
--    where
--        condUnique a = if null args then (S.VarUnique FixVar) else (showStg a)
stgExprToSapl fixVar (StgLit lit)  = emptyRE $ stgLitToSapl fixVar lit
stgExprToSapl fixVar (StgConApp dataCon args) = retExpr mempty (Just ty) $ mkEApp (fixGlobalDataCon fixVar dataCon) (map (stgArgToSapl fixVar) args)   
    where
        ty = dataConTyCon dataCon

stgExprToSapl fixVar (StgOpApp op args _ty) | name == "tagToEnum#" && isBoolTy _ty
	= emptyRE $ mkEApp (S.Var "tagToBool#") (map (stgArgToSapl fixVar) args)
  where
    (S.Var name) = opToVar fixVar op
	
stgExprToSapl fixVar (StgOpApp op args _ty) = emptyRE $ mkEApp (opToVar fixVar op) (map (stgArgToSapl fixVar) args)

#if __GLASGOW_HASKELL__ >= 706
stgExprToSapl fixVar (StgLam bndrs expr)  = stgLamToSapl fixVar bndrs expr
#else
stgExprToSapl fixVar (StgLam _type bndrs expr)  = stgLamToSapl fixVar bndrs expr
#endif

stgExprToSapl fixVar (StgCase expr _liveVars _liveVars' bndr _srt _altType alts) = (exts++pexts,(ty,scase))
    where
        scase = S.ECase sexpr (Just (fixGlobalVar fixVar bndr)) (map (snd . snd) alts')
        (pexts,(_ty,sexpr)) = stgExprToSapl fixVar expr
        alts' = map (stgAltToSapl fixVar) alts
        exts = mconcat . map fst $ alts'
        ty = foldr (appendTy . fst . snd) Nothing alts' 
stgExprToSapl fixVar (StgLet (StgNonRec bndr stgRhs) expr) = (ety++ety',(ty,S.ELet [ds'] ds))
    where
        (ety,(ty,ds)) = stgExprToSapl fixVar expr
        (ety',(_ty',ds')) = letToSapl fixVar bndr stgRhs
stgExprToSapl fixVar (StgLet (StgRec recs) expr) = (ety++ety',(ty,S.ELet ds' ds)) -- second (second (S.ELet ds')) $ stgExprToSapl fixVar expr
    where
        (ety,(ty,ds)) = stgExprToSapl fixVar expr
        rs = map (uncurry (letToSapl fixVar)) recs
        ds' = map (snd . snd) rs
        ety' = mconcat (map fst rs)

stgExprToSapl fixVar (StgLetNoEscape _liveVars _liveVars' binding expr) 
	= (ety,(ty,S.ELetNoEscape nb ne))
	where
		(ety,(ty,S.ELet nb ne)) = stgExprToSapl fixVar (StgLet binding expr)
				
stgExprToSapl fixVar (StgSCC _cc  _b _b' expr) = emptyRE (toNotImplemented fixVar "StgSCC" expr)
stgExprToSapl fixVar (StgTick _mod _i expr) = emptyRE (toNotImplemented fixVar "StgTick" expr)

type LetExpr = (Bool,S.Var,S.Expr)

letToSapl :: FixVar -> Id -> StgRhs -> RetExpr LetExpr
letToSapl fixVar bndr stgRhs = case func' of 
    S.FTFunc (S.FuncName n _num) args e -> (extTy',(ty',(False,n,elam)))
        where
            elam = if null args then e else S.ELam args e
    S.FTCAF (S.FuncName n _num) e -> (extTy',(ty',(False,n,e)))
    S.FTADT n vis -> (extTy',(ty', (False, fixGlobalVar fixVar bndr,mkEApp n (map (S.EVar . fst) vis))))
    where
        Ret extTy' ty' _ func' = stgRhsToSapl fixVar bndr stgRhs

stgLamToSapl :: FixVar -> [Id] -> StgExpr -> RetExpr S.Expr
stgLamToSapl fixVar bndrs expr = second (second (S.ELam (map (fixGlobalVar fixVar) bndrs))) (stgExprToSapl fixVar expr)

stgArgToSapl :: FixVar -> GenStgArg Id -> S.Expr
stgArgToSapl fixVar (StgVarArg occ) = S.EVar (fixLocalVar fixVar $ occ)
stgArgToSapl fixVar (StgLitArg lit) = stgLitToSapl fixVar lit

repInt64 :: Integer -> S.Expr
repInt64 i = S.EApp (S.Var "ghcsapl_num64#") 
			[S.ELit . S.LInt $ toSigned i, S.ELit . S.LInt $ shiftR i 32] 

repWord64 :: Integer -> S.Expr
repWord64 i = S.EApp (S.Var "ghcsapl_num64#") 
			[S.ELit . S.LInt $ toSigned i, S.ELit . S.LInt $ toSigned (shiftR i 32)] 
			
-- make a signed 32 bit int from this unsigned one, lower 32 bits
toSigned :: Integer -> Integer
toSigned i | testBit i 31 = complement (0x7FFFFFFF `xor` (i.&.0x7FFFFFFF))
           | otherwise    = i.&.0xFFFFFFFF		
		
stgLitToSapl :: FixVar -> Literal -> S.Expr
stgLitToSapl _fv (MachChar c) = S.ELit $ S.LChar [c]               -- Char# - at least 31 bits. Create with mkMachChar
stgLitToSapl _fv (MachStr fs) = S.ELit $ S.LString (showOutputable fs)      -- FastString -- A string-literal: stored and emitted UTF-8 encoded, we'll arrange to decode it at runtime. Also emitted with a '\0' terminator. Create with mkMachString
stgLitToSapl _fv MachNullAddr = S.EVar $ S.Var "nullAddr#"
stgLitToSapl _fv (MachInt i) = S.ELit $ S.LInt i                 -- Int# - at least WORD_SIZE_IN_BITS bits. Create with mkMachInt
stgLitToSapl _fv (MachInt64 i) = repInt64 i               --  Integer  -- Int64# - at least 64 bits. Create with mkMachInt64
stgLitToSapl _fv (MachWord i) = S.ELit $ S.LInt i   -- Word# - at least WORD_SIZE_IN_BITS bits. Create with mkMachWord
stgLitToSapl _fv (MachWord64 i) = repWord64 i  -- Word64# - at least 64 bits. Create with mkMachWord64
stgLitToSapl _fv (MachFloat f) = S.ELit $ S.LReal (fromRational f) --  Rational  Float#. Create with mkMachFloat
stgLitToSapl _fv (MachDouble f) = S.ELit $ S.LReal (fromRational f) -- Rational   Double#. Create with mkMachDouble
stgLitToSapl _fv (MachLabel fs _ _) = S.EVar $ S.Var (showOutputable fs)	 --  FastString (Maybe Int) FunctionOrData 
-- A label literal. Parameters:
-- 1) The name of the symbol mentioned in the declaration
-- 2) The size (in bytes) of the arguments the label expects. Only applicable with stdcall labels. Just x => <x> will be appended to label name when emitting assembly.
stgLitToSapl _fv (LitInteger _ _ty) = error "GHCSAPL: LitInteger is not handled"

stgAltToSapl :: FixVar -> StgAlt -> RetExpr (S.Pattern, S.Expr)
stgAltToSapl fixVar (DataAlt con, bndrs, _bools, expr) = first (con:) . second (second (S.PCons conName (map (fixGlobalVar fixVar) bndrs),)) $ stgExprToSapl fixVar expr
    where
        conName = fixGlobalDataCon fixVar con

stgAltToSapl fixVar (LitAlt lit, _bndrs, _bools, expr) = second (second (eLitToPLit (stgLitToSapl fixVar lit),)) $ stgExprToSapl fixVar expr
  where
    eLitToPLit (S.ELit l) = S.PLit l
    eLitToPLit l = error $ "StgToSapl.stgAltToSapl: illegal conversion:" ++ show l
    
stgAltToSapl fixVar (DEFAULT, _bndrs, _bools, expr) = second (second (S.PDefault,)) $ stgExprToSapl fixVar expr

opToVar :: FixVar -> StgOp -> S.Var
opToVar fixVar (StgPrimOp primOp) = fixGlobalPrimOp fixVar primOp     
opToVar fixVar (StgPrimCallOp primCall) = fixGlobalPrimOp fixVar primCall  
opToVar _fixVar (StgFCallOp foreignCall _unique) = S.Var (showOutputable foreignCall)     

appendTy :: Ty -> Ty -> Ty
appendTy Nothing ty = ty
appendTy ty Nothing = ty
appendTy ty _ = ty
   
mkEApp :: S.Var -> [S.Expr] -> S.Expr
mkEApp v@(S.Var s) ls = if cond then S.EApp (S.Var n) (S.EVar f:ls) else tryprim
  where
	n 		= "ffi_wrapper" ++ show (length ls + 1)
	f 		= S.Var . L.last $ es 
	cond 	= length es == 3 && L.isPrefixOf "__pkg_ccall" (head es)
	es 		= L.words s

	tryprim = if cond2 then S.EApp f ls else S.EApp v ls
	cond2	= length es == 3 && "__primcall" == (head es)
	
fixGlobalVar:: FixVar -> Var.Var -> S.Var
fixGlobalVar = fixLocalVar

fixLocalVar:: FixVar -> Var.Var -> S.Var
fixLocalVar fixVar v = if isLocalVar v then mkUniqueVar fixVar v else (fixVar . showOutputable) v 

fixAndShow:: Outputable a => FixVar -> a -> S.Var
fixAndShow fixVar = fixVar . showOutputable

fixGlobalDataCon :: Outputable a => FixVar -> a -> S.Var
fixGlobalDataCon = fixAndShow

fixGlobalPrimOp :: Outputable a => FixVar -> a -> S.Var
fixGlobalPrimOp = fixAndShow


mkUniqueVar :: FixVar -> Var.Var -> S.Var
mkUniqueVar fixVar v = addToVar ((fixAndShow fixVar) v) ('_' : (showOutputable . varUnique) v)

addToVar :: S.Var -> String -> S.Var
addToVar (S.Var v) s = S.Var (v ++ s)

--showStg :: Outputable a => DynFlags -> a -> String
--showStg = showPpr

toNotImplemented :: (Outputable a) => FixVar -> String -> a -> S.Expr
toNotImplemented _fv n s = S.EVar  (S.Var . ("NOT IMPLEMENTED: " ++) . (n ++) . (" " ++) . showOutputable $ s)

