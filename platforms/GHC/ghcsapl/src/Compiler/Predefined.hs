module Compiler.Predefined (
    predefined,
    predefinedADTs
    )
    where

import qualified Data.Map as Map
import qualified Compiler.SAPLStruct as S

predefined :: Map.Map String String
predefined = Map.fromList predeflist

predeflist :: [(String, String)]
predeflist = [(":", "GHC.Types.:"), ("[]", "GHC.Types.[]")] ++ concatMap toPair tuples
	where
		toPair (n,i) = [("(" ++ replicate i ',' ++ ")", n),("(#" ++ replicate i ',' ++ "#)", n)]

tuples :: [(String, Int)]
tuples = [("GHC.Tuple.(" ++ replicate i ',' ++ ")",i) | i<-[0..30]]
	
predefinedADTs :: [S.Func]
predefinedADTs = 
	S.FTADT (S.Var "GHC.Types.[]") [(S.Var "GHC.Types.[]",0),(S.Var "GHC.Types.:",2)]:
	map toFTADT tuples
		where
			toFTADT (n,i) = S.FTADT (S.Var n) [(S.Var n,arity i)]
			arity 0 = 0
			arity i = i + 1