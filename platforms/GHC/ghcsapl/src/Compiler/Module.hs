-- | Read and write JSMods.
module Compiler.Module (writeModule, writeSTG) where


import Module (moduleNameSlashes, mkModuleName, packageIdString, PackageId)

import System.FilePath
import System.Directory


-- | The file extension to use for modules.
jsmodExt :: String
jsmodExt = "sapl"

moduleFilePath :: Maybe FilePath -> String -> String -> FilePath
moduleFilePath basepath _pkgid modname =
  flip addExtension jsmodExt $
    --bpf (pkgid </> (moduleNameSlashes $ mkModuleName modname))
    bpf (moduleNameSlashes $ mkModuleName modname)
      where
      bpf = maybe id (</>) basepath 


writeSTG :: Maybe FilePath -> PackageId -> String -> String -> IO ()
writeSTG basepath pkgid modname srt = do
  createDirectoryIfMissing True (takeDirectory path)
  writeFile (replaceExtension path "stg") srt
  where
    path = moduleFilePath basepath (packageIdString pkgid) modname


writeModule :: Maybe FilePath -> PackageId -> String -> String -> IO ()
writeModule basepath pkgid modname sapl = do
  createDirectoryIfMissing True (takeDirectory path)
  putStrLn $ "writing sapl:" ++ path
  writeFile path sapl
  where
    path = moduleFilePath basepath (packageIdString pkgid) modname

