module Compiler.Lifting (
        
        findFreeVars,
		withVars

    ) where
 
import Control.Monad.State

import qualified Data.Set as Set
import Compiler.SAPLStruct  
import Compiler.TransformerMonad
	
findFreeVars :: Expr -> Set.Set Var -> [Var] -> [Var]
findFreeVars expr declaredVars boundVars
    -- drop variables containing a ".", it must be a function definied in another module
	= filter (\(Var v) -> null $ filter ((==) '.') v) (Set.toList fvs2)
	where
		-- a lambda is possibly a closure, so we need to find free variables and
		-- extend the argument list
		fvs1 = Set.difference (findVariables expr Set.empty) (Set.fromList boundVars) 
		fvs2 = Set.intersection fvs1 declaredVars
		
findVariables :: Expr -> Set.Set Var -> Set.Set Var
findVariables (EApp fn args) vars
	= foldr findVariables (Set.insert fn vars) args

findVariables (ELet binds inbody) vars = withArgs bns inbody afterBindings
	where
		(_, bns, bbs)  = unzip3 binds
		afterBindings  = foldr findVariables vars bbs

findVariables (ELetNoEscape binds inbody) vars = withArgs bns inbody afterBindings
	where
		(_, bns, bbs)  = unzip3 binds
		afterBindings  = foldr findVariables vars bbs
		
findVariables (ELam args body) vars = withArgs args body vars
findVariables (ECase pexpr mbOfName cases) vars = foldr walkPattern vars2 cases
	where
		vars1 = findVariables pexpr vars
		vars2 = maybe vars1 (flip Set.insert vars1) mbOfName
		
findVariables (EIf p l r) vars = findVariables p vars2
	where
		vars1 = findVariables l vars
		vars2 = findVariables r vars1
		
findVariables (EVar var) vars = Set.insert var vars		
findVariables _ vars = vars	

walkPattern :: (Pattern, Expr) -> Set.Set Var -> Set.Set Var
walkPattern ((PCons _ args), body) vars	= withArgs args body vars	 
walkPattern (_, body) vars = findVariables body vars

withArgs :: [Var] -> Expr -> Set.Set Var -> Set.Set Var
withArgs args body vars 
	= Set.difference (findVariables body (Set.union vars argNameSet)) argNameSet	
	where 
		argNameSet = Set.fromList args
	
withVars :: [Var] -> Bool -> State TransformState a -> State TransformState a
withVars vars True expr = do
	st_save  <- get
	modify (\st -> st {boundedVars = Set.union (boundedVars st_save) (Set.fromList vars)})
	ret <- expr
	modify (\st -> st {boundedVars = boundedVars st_save})
	return ret

withVars vars False expr = do
	st_save  <- get
	modify (\st -> st {boundedVars = Set.fromList vars})
	ret <- expr
	modify (\st -> st {boundedVars = boundedVars st_save})
	return ret
					  