module Compiler.Walker (

	walkFun,
	walkExp
	
    ) where

import Control.Monad.State	

import Compiler.SAPLStruct
import Compiler.Lifting (withVars)
import Compiler.TransformerMonad
	
walkFun :: Func -> (Expr -> State TransformState Expr) -> State TransformState Func
walkFun (FTFunc fn args body) f 
	= withVars args False (f body >>= (flip walkExp f >=> return . FTFunc fn args))
	
walkFun (FTCAF fn body) f 
	= withVars [] False (f body >>= (flip walkExp f >=> return . FTCAF fn))

walkFun err@(FTADT{}) _f = error $ "Compiler.SAPLStruct:walkfun: expected pattern " ++ show err

walkExps :: [Expr] -> (Expr -> State TransformState Expr) -> State TransformState [Expr]
walkExps es f = mapM (f >=> flip walkExp f) es

walkExp :: Expr -> (Expr -> State TransformState Expr) -> State TransformState Expr
walkExp (EApp fn args) f =
    do
        newargs <- walkExps args f
        return $ EApp fn newargs
        
walkExp (ELam args body) f 
	= liftM (ELam args) (withVars args True $ f body >>= flip walkExp f)

walkExp (ELet defs inbody) f = do            
    inbody' <- withVars bnames True (f inbody >>= flip walkExp f)
    newbbs <- withVars bnames True (walkExps bbodies f)
    return $ ELet (zip3 annotations bnames newbbs) inbody'
    where		
        (annotations, bnames, bbodies) = unzip3 defs		

walkExp (ELetNoEscape defs inbody) f = do            
    inbody' <- withVars bnames True (f inbody >>= flip walkExp f)
    newbbs <- withVars bnames True (walkExps bbodies f)
    return $ ELetNoEscape (zip3 annotations bnames newbbs) inbody'
    where		
        (annotations, bnames, bbodies) = unzip3 defs		
		
walkExp (EIf ce te fe) f = do
    newce <- f ce >>= flip walkExp f
    newte <- f te >>= flip walkExp f
    newfe <- f fe >>= flip walkExp f
    return $ EIf newce newte newfe

walkExp (ECase caseexpr mbOfName branches) f = do
    ncaseexpr <- (f caseexpr >>= flip walkExp f)
    nbranches <- mapM (walkBranch f) branches
    return $ ECase ncaseexpr mbOfName nbranches
	where
		walkBranch f (pattern, body) = do
			nbody <- withVars (patternVars pattern) True (f body >>= flip walkExp f)
			return (pattern, nbody)
	
walkExp v@(EVar _) f = f v
walkExp e _f = return e

