{-# LANGUAGE TupleSections, FlexibleContexts #-}

module Compiler.Transform (
        
    simplify
	
    ) where

import Control.Monad.State
import Control.Arrow (first)

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Foldable (foldlM)


import Compiler.SAPLStruct
import Compiler.Lifting
import Compiler.TransformerMonad
import Compiler.Walker

simplify :: [Func] -> [Func] -> [Func]
simplify fs ts = evalState (transform fs ts) defaultState        

-- Move top level lambda functions into the function definition
removeTopLevelLambda :: [Func] -> TransformStateMonad 
removeTopLevelLambda fs = return $ map topLift fs
    where
        topLift (FTFunc fn [] (ELam args body)) = FTFunc fn args body
        topLift f = f
		
-- Remove argumentless lambda functions which are used to explicitely create thunks
removeArgumentlessLambdas :: [Func] -> TransformStateMonad 
removeArgumentlessLambdas = mapM (`walkFun` remover)
    where
        remover (ELam [] body) = return body 
        remover e                 = return e

-- 1. Move expression from case to an outer let
--    case expr of var -> let var = expr in case var ...
-- 2. Change case expressions with a sole __DEFAULT branch to a let expression with 
--    a single strict binding
--
-- Post condition: 
-- 1. there is no "case exp of val {_DEFAULT = ...}"
-- 2. there is no "case exp of val {...}" only "case val {...}"
removeExprFromCase :: [Func] -> TransformStateMonad 
removeExprFromCase = mapM (`walkFun` remover)
  where
    remover (ECase caseexpr (Just ofname) [(PDefault, body)]) 
      = return $ ELet [(True, ofname, caseexpr)] body
      
    -- let binding doesn't have to to be strict if caseexp only a variable
    remover (ECase caseexpr@(EApp _ []) (Just ofname) branches) 
      = return $ ELet [(False, ofname, caseexpr)] (ECase (EVar ofname) Nothing branches) 
    remover (ECase caseexpr@(EVar _) (Just ofname) branches) 
      = return $ ELet [(False, ofname, caseexpr)] (ECase (EVar ofname) Nothing branches) 
    
    -- must be strict otherwise: if it is a primitive function, it cannot be inlined unpunished
    remover (ECase caseexpr (Just ofname) branches) 
      = return $ ELet [(True, ofname, caseexpr)] (ECase (EVar ofname) Nothing branches) 
    remover e           = return e


-- Preconditions:
-- 1. It must be after removeExprFromCase in the pipeline (ofname is Nothing)
-- Behavior:
-- 1. Remove definitions like "let a = b in e" where "b" is a sole variable 
--    and substitute "a" for "b" in "e"
-- 2. If all the definitions are gone, remove "let" entirely
-- 3. It my be merged into "removeExprFromCase"
removeTrivialLetDefinitions :: [Func] -> TransformStateMonad 
removeTrivialLetDefinitions = mapM (`walkFun` remover)
    where
        remover (ELet ldefs inbody) = do  
            (ldefs', inbody') <- filterAndSubstitute ldefs inbody
            return (if null ldefs' then inbody' else ELet ldefs' inbody')

        remover e = return e

        filterAndSubstitute ((False, Var bindname, EApp (Var fn) []) : defs) =
            renamer bindname fn >=> 
            flip walkExp (renamer bindname fn) >=> -- TODO: substitute in other defs as well?
            filterAndSubstitute defs 
            
        filterAndSubstitute (def:defs) = filterAndSubstitute defs >=> return . first (def:)
        
        filterAndSubstitute [] = return . ([],)

        renamer from to (EVar fn) 
            = return $ EVar (renvar from to fn)
        renamer from to (EApp fn args) 
            = return $ EApp (renvar from to fn) args --(map (renexp from to) args)
        renamer _ _ e = return e
        
        renvar from to (Var vn) | from == vn = Var to
        renvar _ _ var = var             

exprLifter :: (Expr -> State TransformState Expr) -> [Func] -> TransformStateMonad
exprLifter _ [] = return []
exprLifter lifter (f:fs) =
    do modify (\st -> st {currentFunction = Just f, newFunctions = []})
       nf  <- fixPoint (flip walkFun lifter) f
       st  <- get
       -- reverse: just to be the new function in the good order. actually doesn't matter
       nfs <- exprLifter lifter (reverse(newFunctions st)++fs) 
       return (nf: nfs)

genFuncName :: State TransformState FuncName
genFuncName = do
		st <- get
		let newfn = FuncName (getOriginalFuncName (getFuncName (fromJust' "genFileName" (currentFunction st)))) (nextIndex st)
		modify (\st -> st {nextIndex = nextIndex st + 1})    
		return newfn
		
-- Let-no-escape encodes tail-recursive functions, need special treat
letNoEscapeLifting :: [Func] -> TransformStateMonad 
letNoEscapeLifting = exprLifter lifter
    where
        
		-- the obvious case
		lifter (ELetNoEscape [(False, Var bnv, ELam args bb)] inbody) =
			do 
				st <- get
               
				newfn <- genFuncName
				let realfn = realizeFuncName newfn
		
				let fvs  = findFreeVars bb (boundedVars st) args		
				let fves = map EVar fvs
		
				inbody  <- extend realfn fves inbody
				inbody  <- walkExp inbody (extend realfn fves)
				bb		<- extend realfn fves bb
				bb 		<- walkExp bb (extend realfn fves)		
				
				let newf = FTFunc newfn (fvs++args) bb

				modify (\st -> st {newFunctions = newf: newFunctions st, runAgain = True})

				return $ inbody
			where
				extend v1 args1 (EVar (Var v2)) | v2 == bnv = return (EApp v1 args1)
				extend v1 args1 (EApp (Var v2) args2) | v2 == bnv = return (EApp v1 (args1++args2))
				extend _ _ e = return e
		
		-- TODO: the less obvious case .. convert it to a let for now
		lifter (ELetNoEscape bs inbody) = return (ELet bs inbody)				
		lifter e = return e		
		
-- Lift up lambda expressions (with arguments, the others are aready removed) as top level functions
lambdaLifting :: [Func] -> TransformStateMonad 
lambdaLifting = exprLifter lifter
    where
        
		lifter (ELet defs inbody) = 
			do 
				defs <- mapM (liftDef (Set.fromList $ map snd3 defs)) defs
				return $ ELet defs inbody

		lifter e = return e		
				
		-- TODO: we are interested in only non-strict bindings. I don't remember why??
		liftDef defnames (False, bn, ELam args bb) =
			do 
				st <- get
               
				let fvs = findFreeVars bb (Set.union defnames $ boundedVars st) args

				newfn <- genFuncName
				let newf  = FTFunc newfn (fvs++args) bb

				modify (\st -> st {newFunctions = newf: newFunctions st})
			
				return (False, bn, EApp (realizeFuncName newfn) (map EVar fvs))
		
		liftDef _ e = return e
		
letLifting :: [Func] -> TransformStateMonad 
letLifting = exprLifter lifter
    where
		lifter (ELet defs inbody) = 
			do 
				defs <- mapM (liftDef (Set.fromList $ map snd3 defs)) defs
				return $ ELet defs inbody

		lifter e = return e			
	
		liftDef defnames (strictness, bn, elet@(ELet _ _)) =
			do 
				st <- get

				let fvs = findFreeVars elet (Set.union defnames $ boundedVars st) []

				newfn <- genFuncName
				let newf  = FTFunc newfn fvs elet

				modify (\st -> st {newFunctions = newf: newFunctions st})

				return (strictness, bn, EApp (realizeFuncName newfn) (map EVar fvs))
			   
		liftDef _ e = return e

caseLifting :: [Func] -> TransformStateMonad 
caseLifting = exprLifter lifter
    where
		lifter (ELet defs inbody) = 
			do 
				defs <- mapM (liftDef (Set.fromList $ map snd3 defs)) defs
				return $ ELet defs inbody

		lifter e = return e			

		liftDef defnames (strictness, bn, ecase@(ECase{})) =
			do 
				st <- get

				let fvs = findFreeVars ecase (Set.union defnames $ boundedVars st) []

				newfn <- genFuncName
				let newf  = FTFunc newfn fvs ecase

				modify (\st -> st {newFunctions = newf: newFunctions st})

				return (strictness, bn, EApp (realizeFuncName newfn) (map EVar fvs))

		liftDef _ e = return e
  
-- Preconditions:
-- 1. It must be after removeExprFromCase in the pipeline (ofname is Nothing)
-- Inline let bindings in let body  (repeat until a fixpoint) if:
-- 1. the binding is a non-strict application
-- 2. it is safe: the binding occurs only once in the body
inlineApplication :: [Func] -> TransformStateMonad 
inlineApplication = mapM (fixPoint (`walkFun` inlineDefs))
  where
  
	inlineDefs (ELet defs inbody) =
		do
			(defs, inbody) <- foldlM inliner ([], inbody) defs
  
			if null defs then return inbody else
				return (ELet (reverse defs) inbody)	
  
	inlineDefs e = return e
  
    -- see the comments at "lifter" above 
	inliner (nds, inbody) letdef@(False, bn, app@(EApp _ _)) =
		do 
			modify (\st -> st { modCounter = 0})

			_ <- walkExp app findVar
			st <- get
			
			if modCounter st > 0 then return (letdef:nds, inbody) else do
				newbody <- substitute inbody >>= flip walkExp substitute
				st'     <- get

				-- it is safe to substitute only _once_ otherwise the sharing property is spoiled
				if modCounter st' /= 1 then return (letdef:nds, inbody) else  
					modify (\st'' -> st'' {runAgain = True}) >> return (nds, newbody)
		where
			substitute (EVar v) | bn == v = replace
			substitute (EApp v []) | bn == v = replace
			-- don't substitute into the function part (TODO: the two application could be merged?) 
			substitute e'@(EApp v _) | bn == v =  stop >> return e'
			substitute (ECase (EVar v) Nothing args) | bn == v = increase >> return (ECase app Nothing args)
			substitute e' = return e'

			replace = increase >> return app        
			stop = modify (\st -> st {modCounter = 11})
			increase = modify (\st -> st {modCounter = 1 + modCounter st})

			findVar e'@(EVar v) | bn == v = increase >> return e'
			findVar e'@(EApp v _) | bn == v = increase >> return e'
			findVar e'@(ECase (EVar v) _ _) | bn == v = increase >> return e'
			findVar e' = return e'
    
    -- TODO: strict case: into if or select is ok?  
	inliner (nds, inbody) e = return (e:nds, inbody)
    
        
fixPoint :: MonadState TransformState m => (b -> m b) -> b -> m b
fixPoint f arg = do
   modify (\st -> st {runAgain = False})
   newarg <- f arg
   st     <- get
   if runAgain st
        then fixPoint f newarg
        else return newarg -- modify (\st -> st {runAgain = runAgain oldst}) >> return newarg

-- Boolean is ADT in STG. Convert it to literal. It will be much more efficient
booleanToLiteral :: [Func] -> TransformStateMonad 
booleanToLiteral = mapM (`walkFun` toLit)
    where
        toLit (EApp (Var "GHC.Types.False") []) = return $ ELit (LBool False)
        toLit (EApp (Var "GHC.Types.True") [])  = return $ ELit (LBool True)

        toLit (EVar (Var "GHC.Types.False")) = return $ ELit (LBool False)
        toLit (EVar (Var "GHC.Types.True"))  = return $ ELit (LBool True)
        
        toLit (ECase expr Nothing patterns) = return $ ECase expr Nothing (map toLitInP patterns)
        toLit e = return e
        
        toLitInP (PCons (Var "GHC.Types.False") [], e) = (PLit $ LBool False, e)
        toLitInP (PCons (Var "GHC.Types.True") [], e)  = (PLit $ LBool True,  e)
        toLitInP p = p

-- convert "case" expressions to SAPL "if" if there is no data constructorn case
-- Precondition: 
-- 1. there are at least two cases in the expression
-- 2. case expressions do not contain embedded expressions to evaluate only variables (second arg Nothing)
caseToIf :: [Func] -> TransformStateMonad 
caseToIf = mapM (fixPoint (`walkFun` converter))
    where
        -- The special cases of true-false, false-true
        converter (ECase cond Nothing [(PLit (LBool True),te),(PLit (LBool False),fe)])
            = modify (\ st -> st{runAgain = True}) >> return (EIf cond te fe)
        converter (ECase cond Nothing [(PLit (LBool False),fe),(PLit (LBool True),te)])
            = modify (\ st -> st{runAgain = True}) >> return (EIf cond te fe)

        converter e = return e
      
-- Merge embedded "let" expressions: let bs1 in let bs2 in b -> let bs1,bs2 in b
mergeEmbeddedLets :: [Func] -> TransformStateMonad 
mergeEmbeddedLets = mapM (fixPoint (`walkFun` merger))
    where
        merger (ELet bs1 (ELet bs2 body)) 
            = modify (\ st -> st{runAgain = True}) >> return (ELet (bs1 ++ bs2) body)
        merger e = return e

extractDataConstructors :: [Func] -> TransformStateMonad
extractDataConstructors = mapM toStateADT
    where
        toStateADT f@(FTADT _ defs) =
            do st <- get
               dcs <- toStateCons 0 (length defs) defs (dataCons st)
               modify (\st' -> st' {dataCons = dcs})
               return f
             
        toStateADT f = return f

        toStateCons _ _ [] = return
        toStateCons idx nrCons ((consname, _nrargs):ds)
            = toStateCons (idx+1) nrCons ds . Map.insert consname (idx, nrCons)

transform:: [Func] -> [Func] -> TransformStateMonad
transform fs ts = do
    _ <- extractDataConstructors (filter isADT ts)
    ds' <- extractDataConstructors ds
    fs'' <- pipe fs'
    return (ds' ++ fs'')
    where
        (fs', ds) = (filter isFun fs, filter isADT fs)
        pipe =   
            removeTopLevelLambda        >=> 
            removeArgumentlessLambdas   >=>			
            removeExprFromCase          >=>
            removeTrivialLetDefinitions >=>
            booleanToLiteral            >=>
			-- here we can still substitue into case expressions
            inlineApplication           >=>
			letNoEscapeLifting			>=>
            caseLifting                 >=>             
			lambdaLifting               >=>
            inlineApplication           >=>
            -- "case" can go from "in" to binding, so do it again      
            caseLifting                 >=>
            caseToIf                    >=>
            letLifting                  >=>
            inlineApplication           >=>
            mergeEmbeddedLets
			
fromJust' :: String -> Maybe t -> t
fromJust' _ (Just a) = a
fromJust' mess Nothing = error $ "Transform.fromJust' received nothing, expected:" ++ mess

snd3 :: (a,b,c) -> b
snd3 (_a,b,_c) = b


