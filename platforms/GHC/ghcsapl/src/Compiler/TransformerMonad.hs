module Compiler.TransformerMonad (
        
    TransformState(..),
	TransformStateMonad,
	defaultState

    ) where

import Control.Monad.State

import qualified Data.Set as Set
import qualified Data.Map as Map

import Compiler.SAPLStruct
	
data TransformState = TransformState {

        -- used by lambdaLifting
		  boundedVars	  :: Set.Set Var
        , currentFunction :: Maybe Func
        , newFunctions    :: [Func]
        , nextIndex       :: Int    -- for generating new function names
        
        -- used by inlineApplication andf mergeEmbeddedLets
        , runAgain        :: Bool   -- in fixPoint
        , modCounter      :: Int
        
        -- used by caseToSelect
        , dataCons           :: Map.Map Var (Int, Int) -- fst: constructor index, snd: nrcons in its type
        
        , currentModule   :: String
    }

type TransformStateMonad = State TransformState [Func]
        
defaultState :: TransformState
defaultState = TransformState {
		  boundedVars = Set.empty
        , currentFunction = Nothing
        , newFunctions = [] 
        , nextIndex = 1
        , runAgain = False
        , modCounter = 0
        , dataCons = Map.empty
        , currentModule = ""
    }
