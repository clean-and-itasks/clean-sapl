{-
Warning: don't forget to extend Walker.hs and Lifting.hs if you extend SaplStruct
-}

module Compiler.SAPLStruct (

    Var(..),
    Literal(..),
    Func(..),
    Pattern(..),    
    Expr(..),
	FuncName(..),
	
    
	realizeFuncName,
	getFuncName,
	getOriginalFuncName,
	
	patternVars,    
	fromVar,
    isConstructorPattern,
    isDefaultPattern,
    isFun,
    isADT,
    
    toSAPL

    ) where

import Numeric (showHex)

import Data.List
--import Data.List.Utils -- MissingH package
import Data.Char

data Var 
    = Var String
    deriving (Show, Eq, Ord)
        
data Literal 
    = LString String 
    | LChar String 
    | LInt Integer 
    | LReal Double 
    | LBool Bool
    deriving (Show)

data FuncName = FuncName Var Int
	deriving (Show)
	
data Func 
    = FTFunc FuncName [Var] Expr
    | FTCAF FuncName Expr	
    | FTADT Var [(Var, Int)]
    deriving (Show)

data Pattern 
    = PCons Var [Var] -- constructor name, name of args
    | PLit Literal
    | PDefault
    deriving (Show)
    
data Expr 
    = EApp Var [Expr]
    | ELet [(Bool, Var, Expr)] Expr -- Bool: strictness
    | ELit Literal
    | EVar Var
    | ECase Expr (Maybe Var) [(Pattern, Expr)]

    -- strictly STG stuff
    | ELam [Var] Expr
    | ELetNoEscape [(Bool, Var, Expr)] Expr -- always lazy
    
    -- strictly SAPL stuff
    | EIf Expr Expr Expr
    
    -- transformation failed
    | EFailure String
    
    deriving (Show)

patternVars :: Pattern -> [Var]
patternVars (PCons _ vars) = vars
patternVars _ = []

realizeFuncName :: FuncName -> Var
realizeFuncName (FuncName n 0) = n
realizeFuncName (FuncName n i) = Var $ fromVar n ++ "$" ++ show i

getOriginalFuncName :: FuncName -> Var
getOriginalFuncName (FuncName n _) = n

getFuncName :: Func -> FuncName
getFuncName (FTFunc fn _ _) = fn
getFuncName (FTCAF fn _) = fn
getFuncName (FTADT cname args) = error $ "Compiler.SAPLStruct:getFuncName: expected pattern (FTADT " ++ show cname ++ " " ++ show args ++ ")"

fromVar :: Var -> String
fromVar (Var v) = v

isConstructorPattern :: Pattern -> Bool
isConstructorPattern (PCons _ _) = True
isConstructorPattern _ = False

isDefaultPattern :: Pattern -> Bool
isDefaultPattern PDefault = True
isDefaultPattern _ = False

isFun :: Func -> Bool
isFun (FTADT _ _) = False
isFun _           = True
isADT :: Func -> Bool
isADT (FTADT _ _) = True
isADT _           = False

embed :: (Saplizer a) => a -> String
embed e | isCompound e = "(" ++ toSAPL e ++ ")"
        | otherwise = toSAPL e

pattern :: (Saplizer p, Saplizer e) => (p,e) -> String
pattern (p,e) = "(" ++ toSAPL p ++ " -> " ++ toSAPL e ++ ")"

class Saplizer a where
    toSAPL :: a -> String
    isCompound :: a -> Bool

instance Saplizer Pattern where
    toSAPL (PCons cons args) = toSAPL cons ++ " " ++ unwords (map toSAPL args)
    toSAPL (PLit lit) = toSAPL lit
    toSAPL PDefault = "_"
    isCompound _ = False
    
instance Saplizer Literal where
    toSAPL (LString s)   = "\"" ++ escapeString s ++ "\""
    toSAPL (LChar s)     = "'" ++ escapeString s ++ "'"
    toSAPL (LInt i)      = show i
    toSAPL (LReal r)     = show r
    toSAPL (LBool True)  = "true"
    toSAPL (LBool False) = "false"
    isCompound _ = False
    
instance Saplizer Var where
    toSAPL (Var v) = escapeVar v
    isCompound _ = False

-- | Transform a character to a string that represents its JS
--   unicode escape sequence.
nonPrintableChar :: Char -> String
nonPrintableChar c 
	| cc <= 0xFF
		= ('\\':'x': replicate (2 - length hex) '0') ++ hex
	| cc <= 0xFFFF
		= ('\\':'u': replicate (4 - length hex) '0') ++ hex
	| otherwise
		= ('\\':'U': replicate (8 - length hex) '0') ++ hex
	where
		cc = ord c
		hex = showHex cc ""
  
-- | Correctly replace Haskell characters by the SAPL escape sequences.
escapeString :: String -> String
escapeString [] = []
escapeString (c:cs) = case c of
  -- Backslash has to remain backslash in JS.
  '\\' -> '\\' : '\\' : escapeString cs
  -- Special control sequences.
  '\0' -> '\\' : '0' : escapeString cs
  '\a' -> '\\' : 'a' : escapeString cs
  '\b' -> '\\' : 'b' : escapeString cs
  '\f' -> '\\' : 'f' : escapeString cs
  '\n' -> '\\' : 'n' : escapeString cs
  '\r' -> '\\' : 'r' : escapeString cs
  '\t' -> '\\' : 't' : escapeString cs
  '\v' -> '\\' : 'v' : escapeString cs
  '\"' -> '\\' : '\"' : escapeString cs
  '\'' -> '\\' : '\'' : escapeString cs
  -- Non-control ASCII characters can remain as they are.
  c' | not (isControl c') && isAscii c' -> c' : escapeString cs
  -- All other non ASCII signs are escaped to unicode.
  c' -> nonPrintableChar c' ++ escapeString cs 
	
escapeVar :: String -> String
escapeVar v
    | all (\c -> isAlphaNum c || (c `elem` "_.#$")) v
        = v
    | otherwise 
        = "<{"++v++"}>"
    
instance Saplizer FuncName where
	toSAPL = toSAPL . realizeFuncName 
	isCompound _ = False
	
instance Saplizer Expr where
    toSAPL (EApp f args) = unwords (map embed (EVar f:args))
    toSAPL (ELet bs inbody) = "let " ++ intercalate ", " (map ptos bs) ++ " in " ++ toSAPL inbody   
        where
            ptos (True,  bn, bb) = '!' : ptos (False, bn, bb)
            ptos (False, bn, bb) = toSAPL bn ++ " = " ++ toSAPL bb
    
    toSAPL (ELetNoEscape _ _) = error "ELetNoEscape"	
	
    toSAPL (ELam [] body) = toSAPL body
    toSAPL (ELam args body) = '\\' : unwords (map toSAPL args) ++ " = " ++ toSAPL body
    toSAPL (ELit l) = toSAPL l
    toSAPL (EVar v) = toSAPL v

    toSAPL (EIf c le re) = "if " ++ embed c ++ " " ++ embed le ++ " " ++ embed re
    toSAPL (ECase v _ ps) = "select " ++ embed v ++ " " ++ unwords (map pattern ps)
    
    toSAPL (EFailure s) = "FAILURE: " ++ s
	
    isCompound (ELit l) = isCompound l
    isCompound (EVar v) = isCompound v
    isCompound (ELam [] body) = isCompound body
    isCompound (EApp f []) = isCompound f
    isCompound _ = True
    
instance Saplizer Func where
    toSAPL (FTFunc fn args body) = toSAPL fn ++ " " ++ unwords (map toSAPL args) ++ " = " ++ toSAPL body
    toSAPL (FTCAF fn body) = toSAPL fn ++ " =: " ++ toSAPL body
    toSAPL (FTADT typenm defs) = ":: " ++ toSAPL typenm ++ " = " ++ join " | " (map genDataCons defs)
        where
            join :: [a] -> [[a]] -> [a]
            join delim l = concat (intersperse delim l)
    isCompound _ = True
    
genDataCons :: (Saplizer a) => (a, Int) -> String
genDataCons (cn, arity) = toSAPL cn ++ " " ++ unwords (genArgs arity)

genArgs :: Int -> [String]
genArgs n = take n ['a' : show i | i <- [1..]::[Int]]

