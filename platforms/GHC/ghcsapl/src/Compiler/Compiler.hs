{-# LANGUAGE CPP, TypeFamilies, ScopedTypeVariables, PackageImports, TupleSections #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Compiler.Compiler (
    generate
    ) 
    where

import MonadUtils (MonadIO(..))

import Module (ModuleName)
import DataCon (DataCon,dataConTyCon)

import qualified Compiler.SAPLStruct as S
import qualified Compiler.Transform as T
import Compiler.Predefined (predefined,predefinedADTs)

import StgSyn (StgBinding)
import qualified Data.List as L
import Control.Arrow ((&&&))
import qualified Data.Map as Map

import TyCon (TyCon, visibleDataCons, algTyConRhs)
import DataCon (dataConOrigResTy, dataConRepArity)
import qualified Data.Function as DF
import Data.Maybe (isNothing, fromMaybe)

import Compiler.Utils (showOutputable)

import Compiler.ModGuts (cgGutsToSapl)

generate :: ModuleName
         -> [StgBinding]
         -> IO String
generate modName binds = do
     sapl <- liftIO $ cgGutsToSapl modName binds
     let saplg = L.groupBy ((==) `DF.on` fst) . map snd $ sapl
     let saplADT = concatMap (unsplit . L.foldr split (Nothing,Nothing,[])) saplg :: [(Maybe TyCon,S.Func)]
     let dcs = map (filter (isNothing . flip Map.lookup predefined . showOutputable) . fst) sapl :: [[DataCon]]
     let tfs = map snd sapl :: [(Maybe TyCon,S.Func)]
     let extADTs = predefinedADTs ++ (map mkADT . filt tfs $ dcs)::[S.Func]
     let saplProgram = unlines . map S.toSAPL . flip T.simplify extADTs . map snd $ saplADT
     return saplProgram
  where
    filt :: [(Maybe TyCon,S.Func)] -> [[DataCon]] -> [DataCon]
    filt tyfs dcs = filter ((\ty' -> isNothing $ L.find (uncurry (check ty')) tyfs) . Just . dataConTyCon ) dcsn
        where
            check ty' ty adt = ty' == ty && S.isADT adt
            dcsn = L.nubBy ((==) `DF.on` dataConTyCon) . concat $ dcs :: [DataCon]
    mkADT :: DataCon -> S.Func
    mkADT dc = S.FTADT (S.Var . showOutputable . dataConOrigResTy $ dc) (map (S.Var . showOutputable &&& dataConRepArity) . visibleDataCons . algTyConRhs . dataConTyCon $ dc)

fixPredef :: String -> String    
fixPredef s = fromMaybe s $ Map.lookup s predefined
      
unsplit :: (Maybe TyCon, Maybe S.Func, [S.Func]) -> [(Maybe TyCon, S.Func)]
unsplit (ty,Nothing,fs) = map (ty,) fs
unsplit (ty,Just f,fs) = map (ty,) (f:fs)

split :: (Maybe TyCon,S.Func) -> (Maybe TyCon,Maybe S.Func, [S.Func]) -> (Maybe TyCon, Maybe S.Func, [S.Func])
split (Nothing, S.FTFunc (S.FuncName v@(S.Var n) _) args (S.EApp (S.Var n') _args)) (_,_,l) | n == n' = (Nothing, Nothing, S.FTADT v [(v, length args)]:l)
split (Nothing, S.FTCAF (S.FuncName v@(S.Var n) _) (S.EApp (S.Var n') _args)) (_,_,l) | n == n' = (Nothing, Nothing, S.FTADT v [(v, 0)]:l)
split (Nothing, adt) (_,_,l) = (Nothing, Nothing, adt:l)
split (ty'@(Just ty), S.FTADT (S.Var n) [(S.Var n', 0)]) (_,adt,l) | n == n' = (ty', appendADT (fixPredef . showOutputable $ ty) n 0 adt,l)
split (ty'@(Just _), f@(S.FTADT _ [_])) (_,adt,l) = (ty', adt,f:l)
split (ty'@(Just _), S.FTADT (S.Var n) args) (_,adt,l) = (ty', appendADT (fixPredef . showOutputable $ ty') (err n args) 0 adt,l)
    where 
        err a [] = a ++ "empty data declaration: this shouldn't happen..." 
        err a _ = a ++ "non single argument constructor: this shouldn't happen..."
split (ty'@(Just ty), S.FTFunc (S.FuncName (S.Var n) _) args (S.EApp (S.Var n') _args)) (_,adt,l) | n == n' = (ty', appendADT (fixPredef . showOutputable $ ty) n (length args) adt,l)
split (ty'@(Just ty), S.FTCAF (S.FuncName (S.Var n) _) (S.EApp (S.Var n') _args)) (_,adt,l) | n == n' = (ty', appendADT (fixPredef . showOutputable $ ty) n 0 adt,l)
split (ty'@(Just _), func) (_,adt,l) = (ty', adt,func:l)

appendADT :: String -> String -> Int -> Maybe S.Func -> Maybe S.Func
appendADT tyn an len (Just (S.FTADT v@(S.Var vn) cs)) | vn == tyn = Just (S.FTADT v ((S.Var an,len):cs))
appendADT tyn an len (Just (S.FTADT (S.Var vn) cs)) | vn /= tyn = Just (S.FTADT (S.Var ("non-matching constructors:" ++ tyn ++ " (expected:" ++ vn ++ ")")) ((S.Var an,len):cs))
appendADT tyn an len Nothing = Just (S.FTADT (S.Var tyn) [(S.Var an,len)])
appendADT tyn an len _adt = Just (S.FTADT errVar [(errVar,len)])
    where
        errVar = S.Var ("this shouldn't happen: found S.FTFunc here:" ++ tyn ++ ":" ++ an)

