{-# LANGUAGE CPP, TypeFamilies, ScopedTypeVariables, PackageImports, TupleSections #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Compiler.ModGuts (cgGutsToSapl) where

import Module (ModuleName)

import qualified Compiler.StgToSapl as SToS
import qualified Compiler.SAPLStruct as S

import StgSyn (StgBinding)
import Data.Maybe (catMaybes)


cgGutsToSapl :: ModuleName -> [StgSyn.StgBinding] -> IO [(SToS.ExtTy, (SToS.Ty, S.Func))]
cgGutsToSapl name stg = do 
    let rs = SToS.stgToSapl name (fixList rs) stg
    return . fmap (\ret -> (SToS.extTy ret,(SToS.retTy ret, SToS.func ret))) $ rs
    where
        fixList = catMaybes . map SToS.fixTy


