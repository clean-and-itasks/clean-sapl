{-# LANGUAGE CPP, TypeFamilies, ScopedTypeVariables, PackageImports #-}
module Main where

import GHC
import GHC.Paths (libdir)
import HscMain
import DynFlags hiding (flags)
import TidyPgm
import CorePrep
import CoreToStg
import StgSyn (StgBinding)
import HscTypes
import Module (moduleNameSlashes)
import GhcMonad
import System.Environment (getArgs)

import Version
import System.IO
import System.Process (rawSystem)
import System.Exit (exitFailure)

import Data.Version

import System.FilePath ((</>), replaceExtension)
import System.Directory (doesFileExist)

import Control.Monad (filterM,when,void)
import Data.List (isSuffixOf, intercalate,partition)


logStr :: String -> IO ()
logStr = hPutStrLn stderr

printInfo :: IO ()
printInfo = do
  ghc <- runGhc (Just libdir) getSessionDynFlags
  putStrLn $ formatInfo $ compilerInfo ghc
  where
    formatInfo = ('[' :) . tail . unlines . (++ ["]"]) . map ((',' :) . show)

printNull :: IO ()
printNull = do
  putStrLn "ghcsapl: no input files"
  putStrLn "Usage: For basic information, try the `--help' option."

printUsage :: IO ()
printUsage = do
  putStrLn "Usage: ghcsapl [command-line-options-and-input-files]"
  putStrLn "To compile a complete Haskell program to sapl"
  putStrLn "Command line args similar to ghc. Typical use: ghcsapl --make Main"


-- | Check for arguments concerning version info and the like, and act on them.
--   Return True if the compiler should run afterwards.
preArgs :: [String] -> IO Bool
preArgs args
  | "--numeric-version" `elem` args = putStrLn ghcVersion >> return False
  | null args = printNull >> return False
  | "--help" `elem` args || "-h" `elem` args = printUsage >> return False
  | "--info" `elem` args = printInfo >> return False
  | "--version" `elem` args = putStrLn (showVersion ghcsaplVersion) >> return False
  | "--supported-extensions" `elem` args = (putStrLn $ unlines $ supportedLanguagesAndExtensions) >> return False
  | "--supported-languages" `elem` args = (putStrLn $ unlines $ supportedLanguagesAndExtensions) >> return False
  | otherwise = return True

main :: IO ()
main = do
    args <- fmap (++ packageDBArgs) getArgs
    runCompiler <- preArgs args
    when (runCompiler) $ do
      if allSupported args
        then ghcsaplMain args
        else callVanillaGHC args
  where
    packageDBArgs = []

-- #if __GLASGOW_HASKELL__ >= 706
--    packageDBArgs = ["-no-global-package-db",
--                     "-no-user-package-db",
--                     "-package-db " ++ pkgDir]
-- #else
--    packageDBArgs = ["-no-user-package-conf",
--                     "-package-conf " ++ pkgDir]
-- #endif

-- | Call vanilla GHC; used for boot files and the like.
callVanillaGHC :: [String] -> IO ()
callVanillaGHC = void . rawSystem "ghc" 

-- | Run the compiler if everything's satisfactorily booted, otherwise whine
--   and exit.
ghcsaplMain :: [String] -> IO ()
ghcsaplMain args = do 
  --putStrLn $ "ghcsaplMain:" ++ show args
  compiler args'
  where
    (_, args') = partition ((== "--") . take 2) args

allSupported :: [String] -> Bool
allSupported args =
  and args'
  where
    args' = [not $ any (`isSuffixOf` a) someoneElsesProblems | a <- args]
    someoneElsesProblems = [".c", ".cmm", ".hs-boot", ".lhs-boot"]

-- | The main compiler driver.
compiler :: [String] -> IO ()
compiler cmdargs = do
  let usedGhcMode = if "-c" `elem` cmdargs then OneShot else CompManager


  --putStrLn $ "compiler.right:" ++ show cmdargs
  -- Parse static flags, but ignore profiling.
  (ghcargs', _) <- parseStaticFlags [noLoc a | a <- cmdargs, a /= "-prof"]
  
  runGhc (Just libdir) $ handleSourceError (const $ liftIO exitFailure) $ do

    let args = map unLoc ghcargs'
    dynflags <- getSessionDynFlags
    (dynflags', files, _) <- parseDynamicFlags dynflags (map noLoc args)
    _ <- setSessionDynFlags dynflags' {ghcLink = NoLink,
                                       ghcMode = usedGhcMode}

    -- Prepare and compile all needed targets.
    let files' = map unLoc files
        printErrorAndDie e = printException e >> liftIO exitFailure
    deps <- handleSourceError printErrorAndDie $ do
      ts <- mapM (flip guessTarget Nothing) files'
      setTargets ts
      _ <- load LoadAllTargets

      depanal [] False
    liftIO $ collectFFIjs deps
        

-- | Do everything required to get a list of STG bindings out of a module.
prepare :: (GhcMonad m) => DynFlags -> ModSummary -> m ([StgBinding], ModuleName)
prepare dynflags theMod = do
  env <- getSession
  let name = moduleName $ ms_mod theMod
  pgm <- parseModule theMod
            >>= typecheckModule
            >>= desugarModule
            >>= liftIO . hscSimplify env . coreModule
            >>= liftIO . tidyProgram env
            >>= prepPgm env . fst
            >>= liftIO . coreToStg dynflags
  return (pgm, name)
  where
    prepPgm env tidy = liftIO $ do
#if __GLASGOW_HASKELL__ >= 706
      prepd <- corePrepPgm dynflags env (cg_binds tidy) (cg_tycons tidy)
#else
      prepd <- corePrepPgm dynflags (cg_binds tidy) (cg_tycons tidy)
#endif
      return prepd


collectFFIjs :: [ModSummary] -> IO ()
collectFFIjs [] = return ()
collectFFIjs mgraph = do 
  let filesSL = map nameSL mgraph
      files = map repl filesSL
--   putStrLn ("files:" ++ show files) 
  exs <- filterM doesFileExist files 
  cms <- mapM readFile exs
  when (not . null $ cms) $ do
    let cs = intercalate "\n\n" $ cms
    writeFile ("./ffis/" </> replaceExtension (head filesSL) "ffi.js") cs
  where
    nameSL = moduleNameSlashes . moduleName . ms_mod
    repl f = "./ffis/" </> replaceExtension f "ffipp.js"

