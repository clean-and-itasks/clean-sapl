
module Version (ghcsaplVersion,ghcVersion) where


import Data.Version
import Config (cProjectVersion)

ghcsaplVersion :: Version
ghcsaplVersion = Version [0, 0, 2] []

ghcVersion :: String
ghcVersion = cProjectVersion
