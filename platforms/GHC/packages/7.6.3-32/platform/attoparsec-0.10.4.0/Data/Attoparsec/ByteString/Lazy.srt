[(Data.Attoparsec.ByteString.Lazy.eitherResult
    :: forall r.
       Data.Attoparsec.ByteString.Lazy.Result r
       -> Data.Either.Either GHC.Base.String r
  [GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S, Unf=OtherCon []] =
      \r [ds]
          case ds of _ {
            Data.Attoparsec.ByteString.Lazy.Fail ds1 ds2 msg ->
                Data.Either.Left [msg];
            Data.Attoparsec.ByteString.Lazy.Done ds1 r ->
                Data.Either.Right [r];
          };,
  [(Data.Attoparsec.ByteString.Lazy.eitherResult, [])]),
 (Data.Attoparsec.ByteString.Lazy.maybeResult
    :: forall r.
       Data.Attoparsec.ByteString.Lazy.Result r -> Data.Maybe.Maybe r
  [GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S, Unf=OtherCon []] =
      \r [ds]
          case ds of _ {
            Data.Attoparsec.ByteString.Lazy.Fail ipv ipv1 ipv2 ->
                Data.Maybe.Nothing [];
            Data.Attoparsec.ByteString.Lazy.Done ds1 r -> Data.Maybe.Just [r];
          };,
  [(Data.Attoparsec.ByteString.Lazy.maybeResult, [])]),
 (Data.Attoparsec.ByteString.Lazy.parse_$sgo [Occ=LoopBreaker]
    :: forall r.
       Data.Attoparsec.Internal.Types.IResult
         Data.ByteString.Internal.ByteString r
       -> Data.Attoparsec.ByteString.Lazy.Result r
  [GblId, Arity=1, Str=DmdType S, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [sc]
          case sc of _ {
            Data.Attoparsec.Internal.Types.Fail x stk msg ->
                let {
                  sat_s2AlP :: Data.ByteString.Lazy.Internal.ByteString
                  [LclId] =
                      \u []
                          case x of _ {
                            Data.ByteString.Internal.PS rb rb1 rb2 rb3 ->
                                case rb3 of wild2 {
                                  __DEFAULT ->
                                      Data.ByteString.Lazy.Internal.Chunk [rb
                                                                           rb1
                                                                           rb2
                                                                           wild2
                                                                           Data.ByteString.Lazy.Internal.Empty];
                                  0 -> Data.ByteString.Lazy.Internal.Empty [];
                                };
                          };
                } in  Data.Attoparsec.ByteString.Lazy.Fail [sat_s2AlP stk msg];
            Data.Attoparsec.Internal.Types.Partial k ->
                case k Data.ByteString.empty of sat_s2AlS {
                  __DEFAULT -> Data.Attoparsec.ByteString.Lazy.parse_$sgo sat_s2AlS;
                };
            Data.Attoparsec.Internal.Types.Done x r ->
                let {
                  sat_s2AlT :: Data.ByteString.Lazy.Internal.ByteString
                  [LclId] =
                      \u []
                          case x of _ {
                            Data.ByteString.Internal.PS rb rb1 rb2 rb3 ->
                                case rb3 of wild2 {
                                  __DEFAULT ->
                                      Data.ByteString.Lazy.Internal.Chunk [rb
                                                                           rb1
                                                                           rb2
                                                                           wild2
                                                                           Data.ByteString.Lazy.Internal.Empty];
                                  0 -> Data.ByteString.Lazy.Internal.Empty [];
                                };
                          };
                } in  Data.Attoparsec.ByteString.Lazy.Done [sat_s2AlT r];
          };,
  [(Data.Attoparsec.ByteString.Lazy.parse_$sgo,
    [Data.ByteString.empty,
     Data.Attoparsec.ByteString.Lazy.parse_$sgo])]),
 (Data.Attoparsec.ByteString.Lazy.parse_go [Occ=LoopBreaker]
    :: forall r.
       Data.Attoparsec.Internal.Types.IResult
         Data.ByteString.Internal.ByteString r
       -> Data.ByteString.Lazy.Internal.ByteString
       -> Data.Attoparsec.ByteString.Lazy.Result r
  [GblId, Arity=2, Str=DmdType SL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [ds ys]
          case ds of _ {
            Data.Attoparsec.Internal.Types.Fail x stk msg ->
                let {
                  sat_s2Amm :: Data.ByteString.Lazy.Internal.ByteString
                  [LclId] =
                      \u [] Data.ByteString.Lazy.Internal.chunk x ys;
                } in  Data.Attoparsec.ByteString.Lazy.Fail [sat_s2Amm stk msg];
            Data.Attoparsec.Internal.Types.Partial k ->
                case ys of _ {
                  Data.ByteString.Lazy.Internal.Empty ->
                      case k Data.ByteString.empty of sat_s2Amo {
                        __DEFAULT -> Data.Attoparsec.ByteString.Lazy.parse_$sgo sat_s2Amo;
                      };
                  Data.ByteString.Lazy.Internal.Chunk rb rb1 rb2 rb3 ys1 ->
                      let {
                        sat_s2AiE :: Data.ByteString.Internal.ByteString
                        [LclId] =
                            NO_CCS Data.ByteString.Internal.PS! [rb rb1 rb2 rb3];
                      } in 
                        case k sat_s2AiE of sat_s2Amq {
                          __DEFAULT ->
                              Data.Attoparsec.ByteString.Lazy.parse_go sat_s2Amq ys1;
                        };
                };
            Data.Attoparsec.Internal.Types.Done x r ->
                let {
                  sat_s2Amr :: Data.ByteString.Lazy.Internal.ByteString
                  [LclId] =
                      \u [] Data.ByteString.Lazy.Internal.chunk x ys;
                } in  Data.Attoparsec.ByteString.Lazy.Done [sat_s2Amr r];
          };,
  [(Data.Attoparsec.ByteString.Lazy.parse_go,
    [Data.ByteString.empty, Data.Attoparsec.ByteString.Lazy.parse_go,
     Data.Attoparsec.ByteString.Lazy.parse_$sgo])]),
 (Data.Attoparsec.ByteString.Lazy.parse
    :: forall a.
       Data.Attoparsec.ByteString.Internal.Parser a
       -> Data.ByteString.Lazy.Internal.ByteString
       -> Data.Attoparsec.ByteString.Lazy.Result a
  [GblId, Arity=2, Str=DmdType C(C(C(C(C(S)))))S, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [p s]
          case s of _ {
            Data.ByteString.Lazy.Internal.Empty ->
                case
                    p   Data.ByteString.empty
                        Data.ByteString.Internal.$fMonoidByteString_$cmempty
                        Data.Attoparsec.Internal.Types.Incomplete
                        Data.Attoparsec.ByteString.Internal.failK
                        Data.Attoparsec.ByteString.Internal.successK
                of
                sat_s2AmK
                { __DEFAULT ->
                      Data.Attoparsec.ByteString.Lazy.parse_$sgo sat_s2AmK;
                };
            Data.ByteString.Lazy.Internal.Chunk rb rb1 rb2 rb3 xs ->
                let {
                  sat_s2AmM
                    :: Data.Attoparsec.Internal.Types.Input
                         Data.ByteString.Internal.ByteString
                  [LclId] =
                      NO_CCS Data.ByteString.Internal.PS! [rb rb1 rb2 rb3];
                } in 
                  case
                      p   sat_s2AmM
                          Data.ByteString.Internal.$fMonoidByteString_$cmempty
                          Data.Attoparsec.Internal.Types.Incomplete
                          Data.Attoparsec.ByteString.Internal.failK
                          Data.Attoparsec.ByteString.Internal.successK
                  of
                  sat_s2AmN
                  { __DEFAULT ->
                        Data.Attoparsec.ByteString.Lazy.parse_go sat_s2AmN xs;
                  };
          };,
  [(Data.Attoparsec.ByteString.Lazy.parse,
    [Data.ByteString.empty,
     Data.ByteString.Internal.$fMonoidByteString_$cmempty,
     Data.Attoparsec.ByteString.Lazy.parse_go,
     Data.Attoparsec.ByteString.Lazy.parse_$sgo])]),
 (Data.Attoparsec.ByteString.Lazy.rnfBS [InlPrag=INLINE (sat-args=1),
                                         Occ=LoopBreaker]
    :: Data.ByteString.Lazy.Internal.ByteString -> ()
  [GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S, Unf=OtherCon []] =
      \r [eta]
          case eta of _ {
            Data.ByteString.Lazy.Internal.Empty -> () [];
            Data.ByteString.Lazy.Internal.Chunk rb rb1 rb2 rb3 xs ->
                Data.Attoparsec.ByteString.Lazy.rnfBS xs;
          };,
  [(Data.Attoparsec.ByteString.Lazy.rnfBS, [])]),
 (a :: [GHC.Types.Char] -> ()
  [GblId, Arity=1, Str=DmdType, Unf=OtherCon []] =
      \r [eta_B1]
          Control.DeepSeq.$fNFDataArray_$crnf1
              Control.DeepSeq.$fNFDataChar_$crnf eta_B1;,
  [(a, [])]),
 (Data.Attoparsec.ByteString.Lazy.$fNFDataResult_$crnf [InlPrag=INLINE (sat-args=1)]
    :: forall r.
       Control.DeepSeq.NFData r =>
       Data.Attoparsec.ByteString.Lazy.Result r -> ()
  [GblId, Arity=2, Str=DmdType LS, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dNFData eta]
          case eta of _ {
            Data.Attoparsec.ByteString.Lazy.Fail bs ctxs msg ->
                case Data.Attoparsec.ByteString.Lazy.rnfBS bs of _ {
                  () ->
                      case Control.DeepSeq.$fNFDataArray_$crnf1 a ctxs of _ {
                        () ->
                            Control.DeepSeq.$fNFDataArray_$crnf1
                                Control.DeepSeq.$fNFDataChar_$crnf msg;
                      };
                };
            Data.Attoparsec.ByteString.Lazy.Done bs r ->
                case Data.Attoparsec.ByteString.Lazy.rnfBS bs of _ {
                  () -> $dNFData r;
                };
          };,
  [(Data.Attoparsec.ByteString.Lazy.$fNFDataResult_$crnf, [a])]),
 (Data.Attoparsec.ByteString.Lazy.$fNFDataResult [InlPrag=INLINE (sat-args=0)]
    :: forall r.
       Control.DeepSeq.NFData r =>
       Control.DeepSeq.NFData (Data.Attoparsec.ByteString.Lazy.Result r)
  [GblId[DFunId(nt)], Arity=1, Str=DmdType, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [eta_B1]
          Data.Attoparsec.ByteString.Lazy.$fNFDataResult_$crnf eta_B1;,
  [(Data.Attoparsec.ByteString.Lazy.$fNFDataResult, [a])]),
 (Data.Attoparsec.ByteString.Lazy.$fShowResult2 :: GHC.Types.Char
  [GblId, Caf=NoCafRefs, Unf=OtherCon []] =
      NO_CCS GHC.Types.C#! [' '];,
  [(Data.Attoparsec.ByteString.Lazy.$fShowResult2, [])]),
 (Data.Attoparsec.ByteString.Lazy.$fShowResult1 :: [GHC.Types.Char]
  [GblId, Caf=NoCafRefs, Unf=OtherCon []] =
      NO_CCS :! [GHC.Show.$fShowChar1 GHC.Types.[]];,
  [(Data.Attoparsec.ByteString.Lazy.$fShowResult1, [])]),
 (Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow
    :: forall r.
       GHC.Show.Show r =>
       Data.Attoparsec.ByteString.Lazy.Result r -> GHC.Base.String
  [GblId, Arity=2, Str=DmdType LS, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dShow ds]
          case ds of _ {
            Data.Attoparsec.ByteString.Lazy.Fail bs stk msg ->
                let {
                  sat_s2Anj :: [GHC.Types.Char]
                  [LclId] =
                      \u srt:(0,*bitmap*) []
                          let {
                            sat_s2AjQ :: [GHC.Types.Char]
                            [LclId] =
                                \u srt:(0,*bitmap*) []
                                    let {
                                      sat_s2AjL :: [GHC.Types.Char]
                                      [LclId] =
                                          \u srt:(0,*bitmap*) []
                                              GHC.Show.showLitString
                                                  msg
                                                  Data.Attoparsec.ByteString.Lazy.$fShowResult1; } in
                                    let {
                                      sat_s2AjN :: [GHC.Types.Char]
                                      [LclId] =
                                          NO_CCS :! [GHC.Show.$fShowChar1 sat_s2AjL]; } in
                                    let {
                                      sat_s2Ang :: [GHC.Types.Char]
                                      [LclId] =
                                          NO_CCS :! [Data.Attoparsec.ByteString.Lazy.$fShowResult2
                                                     sat_s2AjN];
                                    } in 
                                      case
                                          GHC.Show.showList__
                                              GHC.Show.$fShowChar_$cshowList stk GHC.Types.[]
                                      of
                                      sat_s2Anh
                                      { __DEFAULT -> GHC.Base.++ sat_s2Anh sat_s2Ang;
                                      }; } in
                          let {
                            sat_s2AjS :: [GHC.Types.Char]
                            [LclId] =
                                NO_CCS :! [Data.Attoparsec.ByteString.Lazy.$fShowResult2
                                           sat_s2AjQ]; } in
                          let {
                            sat_s2AjF :: [GHC.Types.Char]
                            [LclId] =
                                \u srt:(0,*bitmap*) []
                                    case Data.ByteString.Lazy.Internal.unpackChars bs of sat_s2Ani {
                                      __DEFAULT ->
                                          GHC.Show.showLitString
                                              sat_s2Ani
                                              Data.ByteString.Lazy.Internal.$fShowByteString2;
                                    }; } in
                          let {
                            sat_s2AjT :: [GHC.Types.Char]
                            [LclId] =
                                NO_CCS :! [GHC.Show.$fShowChar1 sat_s2AjF];
                          } in  GHC.Base.++ sat_s2AjT sat_s2AjS;
                } in  GHC.CString.unpackAppendCString# "Fail " sat_s2Anj;
            Data.Attoparsec.ByteString.Lazy.Done bs r ->
                let {
                  sat_s2Anm :: [GHC.Types.Char]
                  [LclId] =
                      \u srt:(0,*bitmap*) []
                          let {
                            sat_s2Ak5 :: [GHC.Types.Char]
                            [LclId] =
                                \u [] GHC.Show.show $dShow r; } in
                          let {
                            sat_s2Ak7 :: [GHC.Types.Char]
                            [LclId] =
                                NO_CCS :! [Data.Attoparsec.ByteString.Lazy.$fShowResult2
                                           sat_s2Ak5]; } in
                          let {
                            sat_s2Ak0 :: [GHC.Types.Char]
                            [LclId] =
                                \u srt:(0,*bitmap*) []
                                    case Data.ByteString.Lazy.Internal.unpackChars bs of sat_s2Anl {
                                      __DEFAULT ->
                                          GHC.Show.showLitString
                                              sat_s2Anl
                                              Data.ByteString.Lazy.Internal.$fShowByteString2;
                                    }; } in
                          let {
                            sat_s2Ak8 :: [GHC.Types.Char]
                            [LclId] =
                                NO_CCS :! [GHC.Show.$fShowChar1 sat_s2Ak0];
                          } in  GHC.Base.++ sat_s2Ak8 sat_s2Ak7;
                } in  GHC.CString.unpackAppendCString# "Done " sat_s2Anm;
          };,
  [(Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow,
    [GHC.Show.showLitString, GHC.Show.$fShowChar_$cshowList])]),
 (Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshowsPrec
    :: forall r.
       GHC.Show.Show r =>
       GHC.Types.Int
       -> Data.Attoparsec.ByteString.Lazy.Result r -> GHC.Show.ShowS
  [GblId, Arity=4, Str=DmdType LASL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dShow ds x s]
          case
              Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow $dShow x
          of
          sat_s2Ann
          { __DEFAULT -> GHC.Base.++ sat_s2Ann s;
          };,
  [(Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshowsPrec,
    [Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow])]),
 (Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshowList
    :: forall r.
       GHC.Show.Show r =>
       [Data.Attoparsec.ByteString.Lazy.Result r] -> GHC.Show.ShowS
  [GblId, Arity=3, Str=DmdType LSL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dShow eta eta1]
          let {
            sat_s2Anp
              :: Data.Attoparsec.ByteString.Lazy.Result r -> GHC.Show.ShowS
            [LclId] =
                \r srt:(0,*bitmap*) [x s]
                    case
                        Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow $dShow x
                    of
                    sat_s2Ano
                    { __DEFAULT -> GHC.Base.++ sat_s2Ano s;
                    };
          } in  GHC.Show.showList__ sat_s2Anp eta eta1;,
  [(Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshowList,
    [Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow])]),
 (Data.Attoparsec.ByteString.Lazy.$fShowResult [InlPrag=[ALWAYS] CONLIKE]
    :: forall r.
       GHC.Show.Show r =>
       GHC.Show.Show (Data.Attoparsec.ByteString.Lazy.Result r)
  [GblId[DFunId], Arity=1, Str=DmdType Lm, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dShow]
          let {
            sat_s2Anq
              :: [Data.Attoparsec.ByteString.Lazy.Result r] -> GHC.Show.ShowS
            [LclId] =
                \r srt:(0,*bitmap*) [eta_B2 eta_B1]
                    Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshowList
                        $dShow eta_B2 eta_B1; } in
          let {
            sat_s2Anr
              :: Data.Attoparsec.ByteString.Lazy.Result r -> GHC.Base.String
            [LclId] =
                \r srt:(1,*bitmap*) [eta_B1]
                    Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow
                        $dShow eta_B1; } in
          let {
            sat_s2Ans
              :: GHC.Types.Int
                 -> Data.Attoparsec.ByteString.Lazy.Result r -> GHC.Show.ShowS
            [LclId] =
                \r srt:(2,*bitmap*) [eta_B3 eta_B2 eta_B1]
                    Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshowsPrec
                        $dShow eta_B3 eta_B2 eta_B1;
          } in  GHC.Show.D:Show [sat_s2Ans sat_s2Anr sat_s2Anq];,
  [(Data.Attoparsec.ByteString.Lazy.$fShowResult,
    [Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow,
     Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow,
     Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow])]),
 (Data.Attoparsec.ByteString.Lazy.parseTest1
    :: forall a.
       GHC.Show.Show a =>
       Data.Attoparsec.ByteString.Internal.Parser a
       -> Data.ByteString.Lazy.Internal.ByteString
       -> GHC.Prim.State# GHC.Prim.RealWorld
       -> (# GHC.Prim.State# GHC.Prim.RealWorld, () #)
  [GblId, Arity=4, Str=DmdType LLLL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dShow p s eta]
          let {
            sat_s2Anu :: GHC.Base.String
            [LclId] =
                \u srt:(1,*bitmap*) []
                    case s of _ {
                      Data.ByteString.Lazy.Internal.Empty ->
                          case
                              p   Data.ByteString.empty
                                  Data.ByteString.Internal.$fMonoidByteString_$cmempty
                                  Data.Attoparsec.Internal.Types.Incomplete
                                  Data.Attoparsec.ByteString.Internal.failK
                                  Data.Attoparsec.ByteString.Internal.successK
                          of
                          sat_s2AkJ
                          { __DEFAULT ->
                                case
                                    Data.Attoparsec.ByteString.Lazy.parse_$sgo sat_s2AkJ
                                of
                                sat_s2Ant
                                { __DEFAULT ->
                                      Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow
                                          $dShow sat_s2Ant;
                                };
                          };
                      Data.ByteString.Lazy.Internal.Chunk rb rb1 rb2 rb3 xs ->
                          let {
                            sat_s2Anw
                              :: Data.Attoparsec.Internal.Types.Input
                                   Data.ByteString.Internal.ByteString
                            [LclId] =
                                NO_CCS Data.ByteString.Internal.PS! [rb rb1 rb2 rb3];
                          } in 
                            case
                                p   sat_s2Anw
                                    Data.ByteString.Internal.$fMonoidByteString_$cmempty
                                    Data.Attoparsec.Internal.Types.Incomplete
                                    Data.Attoparsec.ByteString.Internal.failK
                                    Data.Attoparsec.ByteString.Internal.successK
                            of
                            sat_s2Anx
                            { __DEFAULT ->
                                  case
                                      Data.Attoparsec.ByteString.Lazy.parse_go sat_s2Anx xs
                                  of
                                  sat_s2Any
                                  { __DEFAULT ->
                                        Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow
                                            $dShow sat_s2Any;
                                  };
                            };
                    };
          } in 
            GHC.IO.Handle.Text.hPutStr2
                GHC.IO.Handle.FD.stdout sat_s2Anu GHC.Types.True eta;,
  [(Data.Attoparsec.ByteString.Lazy.parseTest1,
    [GHC.IO.Handle.FD.stdout, Data.ByteString.empty,
     Data.ByteString.Internal.$fMonoidByteString_$cmempty,
     GHC.IO.Handle.Text.hPutStr2,
     Data.Attoparsec.ByteString.Lazy.parse_go,
     Data.Attoparsec.ByteString.Lazy.parse_$sgo,
     Data.Attoparsec.ByteString.Lazy.$fShowResult_$cshow])]),
 (Data.Attoparsec.ByteString.Lazy.parseTest
    :: forall a.
       GHC.Show.Show a =>
       Data.Attoparsec.ByteString.Internal.Parser a
       -> Data.ByteString.Lazy.Internal.ByteString -> GHC.Types.IO ()
  [GblId, Arity=4, Str=DmdType LLLL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [eta_B4 eta_B3 eta_B2 eta_B1]
          Data.Attoparsec.ByteString.Lazy.parseTest1
              eta_B4 eta_B3 eta_B2 eta_B1;,
  [(Data.Attoparsec.ByteString.Lazy.parseTest,
    [Data.Attoparsec.ByteString.Lazy.parseTest1])]),
 (Data.Attoparsec.ByteString.Lazy.$fFunctorResult_$cfmap
    :: forall a b.
       (a -> b)
       -> Data.Attoparsec.ByteString.Lazy.Result a
       -> Data.Attoparsec.ByteString.Lazy.Result b
  [GblId, Arity=2, Caf=NoCafRefs, Str=DmdType LS, Unf=OtherCon []] =
      \r [ds ds1]
          case ds1 of _ {
            Data.Attoparsec.ByteString.Lazy.Fail st stk msg ->
                Data.Attoparsec.ByteString.Lazy.Fail [st stk msg];
            Data.Attoparsec.ByteString.Lazy.Done bs r ->
                let {
                  sat_s2AnA :: b
                  [LclId] =
                      \u [] ds r;
                } in  Data.Attoparsec.ByteString.Lazy.Done [bs sat_s2AnA];
          };,
  [(Data.Attoparsec.ByteString.Lazy.$fFunctorResult_$cfmap, [])]),
 (Data.Attoparsec.ByteString.Lazy.$fFunctorResult_$c<$
    :: forall a b.
       a
       -> Data.Attoparsec.ByteString.Lazy.Result b
       -> Data.Attoparsec.ByteString.Lazy.Result a
  [GblId, Arity=2, Caf=NoCafRefs, Str=DmdType LS, Unf=OtherCon []] =
      \r [x eta]
          case eta of _ {
            Data.Attoparsec.ByteString.Lazy.Fail st stk msg ->
                Data.Attoparsec.ByteString.Lazy.Fail [st stk msg];
            Data.Attoparsec.ByteString.Lazy.Done bs r ->
                Data.Attoparsec.ByteString.Lazy.Done [bs x];
          };,
  [(Data.Attoparsec.ByteString.Lazy.$fFunctorResult_$c<$, [])]),
 (Data.Attoparsec.ByteString.Lazy.$fFunctorResult [InlPrag=[ALWAYS] CONLIKE]
    :: GHC.Base.Functor Data.Attoparsec.ByteString.Lazy.Result
  [GblId[DFunId], Caf=NoCafRefs, Str=DmdType m, Unf=OtherCon []] =
      NO_CCS GHC.Base.D:Functor! [Data.Attoparsec.ByteString.Lazy.$fFunctorResult_$cfmap
                                  Data.Attoparsec.ByteString.Lazy.$fFunctorResult_$c<$];,
  [(Data.Attoparsec.ByteString.Lazy.$fFunctorResult, [])]),
 (Data.Attoparsec.ByteString.Lazy.Fail
    :: forall r.
       Data.ByteString.Lazy.Internal.ByteString
       -> [GHC.Base.String]
       -> GHC.Base.String
       -> Data.Attoparsec.ByteString.Lazy.Result r
  [GblId[DataCon],
   Arity=3,
   Caf=NoCafRefs,
   Str=DmdType TTT,
   Unf=OtherCon []] =
      \r [eta_B3 eta_B2 eta_B1]
          Data.Attoparsec.ByteString.Lazy.Fail [eta_B3 eta_B2 eta_B1];,
  [(Data.Attoparsec.ByteString.Lazy.Fail, [])]),
 (Data.Attoparsec.ByteString.Lazy.Done
    :: forall r.
       Data.ByteString.Lazy.Internal.ByteString
       -> r -> Data.Attoparsec.ByteString.Lazy.Result r
  [GblId[DataCon],
   Arity=2,
   Caf=NoCafRefs,
   Str=DmdType TT,
   Unf=OtherCon []] =
      \r [eta_B2 eta_B1]
          Data.Attoparsec.ByteString.Lazy.Done [eta_B2 eta_B1];,
  [(Data.Attoparsec.ByteString.Lazy.Done, [])])]