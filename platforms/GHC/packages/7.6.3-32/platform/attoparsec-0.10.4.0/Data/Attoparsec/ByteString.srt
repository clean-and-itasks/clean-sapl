[(Data.Attoparsec.ByteString.eitherResult2 :: [GHC.Types.Char]
  [GblId, Str=DmdType] =
      \u [] GHC.CString.unpackCString# "Result: incomplete input";,
  [(Data.Attoparsec.ByteString.eitherResult2, [])]),
 (Data.Attoparsec.ByteString.eitherResult1
    :: forall r. Data.Either.Either GHC.Base.String r
  [GblId, Str=DmdType, Unf=OtherCon []] =
      NO_CCS Data.Either.Left! [Data.Attoparsec.ByteString.eitherResult2];,
  [(Data.Attoparsec.ByteString.eitherResult1, [])]),
 (Data.Attoparsec.ByteString.eitherResult
    :: forall r.
       Data.Attoparsec.ByteString.Internal.Result r
       -> Data.Either.Either GHC.Base.String r
  [GblId, Arity=1, Str=DmdType S, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [ds]
          case ds of _ {
            Data.Attoparsec.Internal.Types.Fail ds1 ds2 msg ->
                Data.Either.Left [msg];
            Data.Attoparsec.Internal.Types.Partial ipv ->
                Data.Attoparsec.ByteString.eitherResult1;
            Data.Attoparsec.Internal.Types.Done ds1 r -> Data.Either.Right [r];
          };,
  [(Data.Attoparsec.ByteString.eitherResult,
    [Data.Attoparsec.ByteString.eitherResult1])]),
 (Data.Attoparsec.ByteString.feed [InlPrag=INLINE (sat-args=2)]
    :: forall r.
       Data.Attoparsec.ByteString.Internal.Result r
       -> Data.ByteString.Internal.ByteString
       -> Data.Attoparsec.ByteString.Internal.Result r
  [GblId, Arity=2, Str=DmdType SL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [eta eta1]
          case eta of wild {
            Data.Attoparsec.Internal.Types.Fail ds ds1 ds2 -> wild;
            Data.Attoparsec.Internal.Types.Partial k -> k eta1;
            Data.Attoparsec.Internal.Types.Done bs r ->
                let {
                  sat_s25Yo :: Data.ByteString.Internal.ByteString
                  [LclId] =
                      \u srt:(0,*bitmap*) []
                          Data.ByteString.Internal.$fMonoidByteString_$cmappend bs eta1;
                } in  Data.Attoparsec.Internal.Types.Done [sat_s25Yo r];
          };,
  [(Data.Attoparsec.ByteString.feed,
    [Data.ByteString.Internal.$fMonoidByteString_$cmappend])]),
 (Data.Attoparsec.ByteString.maybeResult
    :: forall r.
       Data.Attoparsec.ByteString.Internal.Result r -> Data.Maybe.Maybe r
  [GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S, Unf=OtherCon []] =
      \r [ds]
          case ds of _ {
            __DEFAULT -> Data.Maybe.Nothing [];
            Data.Attoparsec.Internal.Types.Done ds1 r -> Data.Maybe.Just [r];
          };,
  [(Data.Attoparsec.ByteString.maybeResult, [])]),
 (Data.Attoparsec.ByteString.parseTest1
    :: forall a.
       GHC.Show.Show a =>
       Data.Attoparsec.ByteString.Internal.Parser a
       -> Data.ByteString.Internal.ByteString
       -> GHC.Prim.State# GHC.Prim.RealWorld
       -> (# GHC.Prim.State# GHC.Prim.RealWorld, () #)
  [GblId, Arity=4, Str=DmdType LLLL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dShow p s eta]
          let {
            sat_s25YD :: GHC.Base.String
            [LclId] =
                \u srt:(1,*bitmap*) []
                    case
                        p   s
                            Data.ByteString.Internal.$fMonoidByteString_$cmempty
                            Data.Attoparsec.Internal.Types.Incomplete
                            Data.Attoparsec.ByteString.Internal.failK
                            Data.Attoparsec.ByteString.Internal.successK
                    of
                    sat_s25YC
                    { __DEFAULT ->
                          Data.Attoparsec.Internal.Types.$fShowIResult_$cshow
                              Data.ByteString.Internal.$fShowByteString $dShow sat_s25YC;
                    };
          } in 
            GHC.IO.Handle.Text.hPutStr2
                GHC.IO.Handle.FD.stdout sat_s25YD GHC.Types.True eta;,
  [(Data.Attoparsec.ByteString.parseTest1,
    [GHC.IO.Handle.FD.stdout,
     Data.ByteString.Internal.$fShowByteString,
     Data.ByteString.Internal.$fMonoidByteString_$cmempty,
     Data.Attoparsec.Internal.Types.$fShowIResult_$cshow,
     GHC.IO.Handle.Text.hPutStr2])]),
 (Data.Attoparsec.ByteString.parseTest
    :: forall a.
       GHC.Show.Show a =>
       Data.Attoparsec.ByteString.Internal.Parser a
       -> Data.ByteString.Internal.ByteString -> GHC.Types.IO ()
  [GblId, Arity=4, Str=DmdType LLLL, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [eta_B4 eta_B3 eta_B2 eta_B1]
          Data.Attoparsec.ByteString.parseTest1 eta_B4 eta_B3 eta_B2 eta_B1;,
  [(Data.Attoparsec.ByteString.parseTest,
    [Data.Attoparsec.ByteString.parseTest1])]),
 (Data.Attoparsec.ByteString.parseWith [InlPrag=INLINE (sat-args=3)]
    :: forall (m :: * -> *) a.
       GHC.Base.Monad m =>
       m Data.ByteString.Internal.ByteString
       -> Data.Attoparsec.ByteString.Internal.Parser a
       -> Data.ByteString.Internal.ByteString
       -> m (Data.Attoparsec.ByteString.Internal.Result a)
  [GblId,
   Arity=4,
   Str=DmdType U(LALA)LC(C(C(C(C(S)))))L,
   Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dMonad eta eta1 eta2]
          let {
            lvl
              :: m Data.ByteString.Internal.ByteString
                 -> (Data.ByteString.Internal.ByteString
                     -> m (Data.Attoparsec.Internal.Types.IResult
                             Data.ByteString.Internal.ByteString a))
                 -> m (Data.Attoparsec.Internal.Types.IResult
                         Data.ByteString.Internal.ByteString a)
            [LclId] =
                \u [] GHC.Base.>>= $dMonad; } in
          let {
            lvl1
              :: Data.Attoparsec.Internal.Types.IResult
                   Data.ByteString.Internal.ByteString a
                 -> m (Data.Attoparsec.Internal.Types.IResult
                         Data.ByteString.Internal.ByteString a)
            [LclId] =
                \u [] GHC.Base.return $dMonad; } in
          let {
            step [Occ=LoopBreaker]
              :: Data.Attoparsec.Internal.Types.IResult
                   Data.ByteString.Internal.ByteString a
                 -> m (Data.Attoparsec.Internal.Types.IResult
                         Data.ByteString.Internal.ByteString a)
            [LclId, Arity=1, Str=DmdType S, Unf=OtherCon []] =
                sat-only \r [ds]
                    case ds of wild {
                      __DEFAULT -> lvl1 wild;
                      Data.Attoparsec.Internal.Types.Partial k ->
                          let {
                            sat_s25YM
                              :: Data.ByteString.Internal.ByteString
                                 -> m (Data.Attoparsec.Internal.Types.IResult
                                         Data.ByteString.Internal.ByteString a)
                            [LclId] =
                                \r [x] case k x of sat_s25YL { __DEFAULT -> step sat_s25YL; };
                          } in  lvl eta sat_s25YM;
                    };
          } in 
            case
                Data.Attoparsec.ByteString.Internal.parse eta1 eta2
            of
            sat_s25YK
            { __DEFAULT -> step sat_s25YK;
            };,
  [(Data.Attoparsec.ByteString.parseWith,
    [Data.Attoparsec.ByteString.Internal.parse])])]