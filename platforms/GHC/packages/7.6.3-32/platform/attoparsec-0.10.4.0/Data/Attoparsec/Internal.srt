[(Data.Attoparsec.Internal.compareResults_$dEq
    :: GHC.Classes.Eq [GHC.Types.Char]
  [GblId, Str=DmdType] =
      \u [] GHC.Classes.$fEq[] GHC.Classes.$fEqChar;,
  [(Data.Attoparsec.Internal.compareResults_$dEq, [])]),
 (Data.Attoparsec.Internal.compareResults1
    :: Data.Maybe.Maybe GHC.Types.Bool
  [GblId, Caf=NoCafRefs, Str=DmdType, Unf=OtherCon []] =
      NO_CCS Data.Maybe.Just! [GHC.Types.False];,
  [(Data.Attoparsec.Internal.compareResults1, [])]),
 (Data.Attoparsec.Internal.compareResults
    :: forall t r.
       (GHC.Classes.Eq t, GHC.Classes.Eq r) =>
       Data.Attoparsec.Internal.Types.IResult t r
       -> Data.Attoparsec.Internal.Types.IResult t r
       -> Data.Maybe.Maybe GHC.Types.Bool
  [GblId, Arity=4, Str=DmdType LLSS, Unf=OtherCon []] =
      \r srt:(0,*bitmap*) [$dEq $dEq1 ds ds1]
          case ds of _ {
            Data.Attoparsec.Internal.Types.Fail i0 ctxs0 msg0 ->
                case ds1 of _ {
                  __DEFAULT -> Data.Attoparsec.Internal.compareResults1;
                  Data.Attoparsec.Internal.Types.Fail i1 ctxs1 msg1 ->
                      let {
                        sat_sbHA :: GHC.Types.Bool
                        [LclId] =
                            \u srt:(0,*bitmap*) []
                                case GHC.Classes.== $dEq i0 i1 of _ {
                                  GHC.Types.False -> GHC.Types.False [];
                                  GHC.Types.True ->
                                      case
                                          GHC.Classes.$fEq[]_$c==
                                              Data.Attoparsec.Internal.compareResults_$dEq
                                              ctxs0
                                              ctxs1
                                      of
                                      _
                                      { GHC.Types.False -> GHC.Types.False [];
                                        GHC.Types.True -> GHC.Base.eqString msg0 msg1;
                                      };
                                };
                      } in  Data.Maybe.Just [sat_sbHA];
                };
            Data.Attoparsec.Internal.Types.Partial ds2 ->
                case ds1 of _ {
                  __DEFAULT -> Data.Attoparsec.Internal.compareResults1;
                  Data.Attoparsec.Internal.Types.Partial ds3 ->
                      Data.Maybe.Nothing [];
                };
            Data.Attoparsec.Internal.Types.Done i0 r0 ->
                case ds1 of _ {
                  __DEFAULT -> Data.Attoparsec.Internal.compareResults1;
                  Data.Attoparsec.Internal.Types.Done i1 r1 ->
                      let {
                        sat_sbHH :: GHC.Types.Bool
                        [LclId] =
                            \u []
                                case GHC.Classes.== $dEq i0 i1 of _ {
                                  GHC.Types.False -> GHC.Types.False [];
                                  GHC.Types.True -> GHC.Classes.== $dEq1 r0 r1;
                                };
                      } in  Data.Maybe.Just [sat_sbHH];
                };
          };,
  [(Data.Attoparsec.Internal.compareResults,
    [Data.Attoparsec.Internal.compareResults_$dEq])])]