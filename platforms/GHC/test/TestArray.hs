module TestArray (main) where

import Data.Array

default(Int)

test1 = array (1,10) ((1,1) : [(i, i * test1!(i-1)) | i <- [2..10]])

wavefront :: Int -> Array (Int,Int) Int
wavefront n	=  a  where
					a = array ((1,1),(n,n))
						([((1,j), 1) | j <- [1..n]] ++
						 [((i,1), 1) | i <- [2..n]] ++
						 [((i,j), a!(i,j-1) + a!(i-1,j-1) + a!(i-1,j))
							| i <- [2..n], j <- [2..n]])

matMult x y =  accumArray (+) 0 resultBounds
		[((i,j), x!(i,k) * y!(k,j))	| i <- range (li,ui), j <- range (lj',uj'), k <- range (lj,uj)]
		where 
			((li,lj),(ui,uj))		=  bounds x
			((li',lj'),(ui',uj'))	=  bounds y
			resultBounds
				| (lj,uj)==(li',ui')	=  ((li,lj'),(ui,uj'))
				| otherwise				= error "matMult: incompatible bounds"

a = array ((1,1),(4,4)) [((1,1),1),((1,2),1),((1,3),1),((1,4),1),((2,1),1),((2,2),3),
		  ((2,3),5),((2,4),7),((3,1),1),((3,2),5),((3,3),13),((3,4),25),((4,1),1),
		  ((4,2),7),((4,3),25),((4,4),63)]

b = array ((1,1),(4,4)) [((1,1),1),((1,2),1),((1,3),1),((1,4),1),((2,1),1),((2,2),3),
		  ((2,3),5),((2,4),7),((3,1),1),((3,2),5),((3,3),13),((3,4),25),((4,1),1),
		  ((4,2),7),((4,3),25),((4,4),63)]
		  
main = do
	print test1
	print (wavefront 4)
	print (matMult a b)
	