module TestInteger (main) where

import Data.Int
import Data.Word
import Numeric

ismall :: Integer
ismall = 123

i1 :: Integer
i1 = 0x1

im20 :: Integer
im20 = -20

i3 :: Integer
i3 = 3

main = do
	putStr "0x"
	putStrLn $ showHex (ismall+i1+i1) ""
	
	putStrLn (show $ im20 `divMod` i3 == (-7,1))
	putStrLn (show $ im20 `divMod` i3)
	putStrLn (show $ im20 `quotRem` i3 == (-6,-2))
	putStrLn (show $ im20 `quotRem` i3)
		