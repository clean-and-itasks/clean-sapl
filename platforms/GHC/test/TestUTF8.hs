﻿module TestUTF8 (main) where

import Data.Word
import Data.List
import qualified Data.String.UTF8 as UTF8
import Codec.Binary.UTF8.String

utf2 :: UTF8.UTF8 [Word8]
utf2 = UTF8.fromString "László"

main = do
	print "áéíő"
	putStrLn "áéíő"
	print $ UTF8.length utf2
	print $ length (UTF8.toRep utf2)
	print $ UTF8.toString utf2
	putStrLn $ UTF8.toString utf2
	
	print ("\x03ba\x1f79\x03c3\x03bc\x03b5 " == decode [0xce,0xba,0xe1,0xbd,0xb9,0xcf,0x83,0xce,0xbc,0xce,0xb5,0x20]) 