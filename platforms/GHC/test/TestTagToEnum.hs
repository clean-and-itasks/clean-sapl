{-# LANGUAGE MagicHash, BangPatterns #-}
module TestTagToEnum where
import GHC.Prim
import GHC.Types

data D = A | B | C deriving Show

{-# NOINLINE foo #-}
foo :: Int
foo = 1

{-# NOINLINE bar #-}
bar :: Int
bar = 2

{-# NOINLINE z1 #-}
z1 :: Int
z1 = 0

tagToEnumB :: Int -> Bool
tagToEnumB (I# x) = tagToEnum# x

tagToEnum :: Int -> D
tagToEnum (I# x) = tagToEnum# x

dataToTag :: a -> Int
dataToTag x = I# (dataToTag# x)

main :: IO ()
main = do
    print  (tagToEnum foo,
            tagToEnum bar,
            tagToEnumB z1,
            tagToEnum $ dataToTag A)
