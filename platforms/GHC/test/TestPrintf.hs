module TestPrintf (main) where

import Text.Printf
import GHC.Float

x1 :: Double
x1 = 1.14907259

x2 :: Float
x2 = 1.14907259

main = do
	printf "%.2f" $ x1
	print ", "
	printf "%.2f" $ x2
	
	