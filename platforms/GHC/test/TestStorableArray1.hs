module TestStorableArray1 (main) where

import Data.Array.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.ForeignPtr
 
main = do 
			ptr <- mallocArray 10
			fptr <- newForeignPtr_ ptr
			arr <- unsafeForeignPtrToStorableArray fptr (1,10)  :: IO (StorableArray Int Int)
			writeArray arr 1 64
			a <- readArray arr 1
			print a
			free ptr