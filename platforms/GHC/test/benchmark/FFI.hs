{-# LANGUAGE ForeignFunctionInterface, TypeFamilies, MagicHash, TypeSynonymInstances, FlexibleInstances #-}

module Main (main) where

import Prelude

import Unsafe.Coerce(unsafeCoerce)

import Prelude(String,IO,(>>=),(>>),(.),show,return,putStrLn,seq,($!));

default(Int);

foreign import ccall "jsAlertString"
  jsAlertString :: Ptr String -> IO ()

data FakePtr a = FakePtr a

toPtr :: a -> Ptr a
toPtr = unsafeCoerce . FakePtr

main = return "bcd" >>= \s -> return ('a':s) >>= return . toPtr >>= jsAlertString
