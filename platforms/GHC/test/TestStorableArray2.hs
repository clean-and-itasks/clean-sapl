module TestStorableArray2 (main) where

{-# OPTIONS_GHC -fglasgow-exts #-}
import Data.Array.Storable
import Foreign.Ptr
import Foreign.C.Types

main = do 
			arr <- newArray (1,10) 37 :: IO (StorableArray Int Int)
			a <- readArray arr 1
			withStorableArray arr (\ptr -> memset ptr 0 40)
			b <- readArray arr 1
			print (a,b)

foreign import ccall unsafe "string.h" 
	memset  :: Ptr a -> CInt -> CSize -> IO ()