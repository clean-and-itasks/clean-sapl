module TestBinary (main) where

import Data.Binary
import Control.Monad
--import Codec.Compression.GZip

-- A Haskell AST structure
data Exp = IntE Int
         | OpE  String Exp Exp
   deriving Eq

-- An instance of Binary to encode and decode an Exp in binary
instance Binary Exp where
     put (IntE i)          = put (0 :: Word8) >> put i
     put (OpE s e1 e2)     = put (1 :: Word8) >> put s >> put e1 >> put e2
     get = do tag <- getWord8
              case tag of
                  0 -> liftM  IntE get
                  1 -> liftM3 OpE  get get get

-- A test expression
e = OpE "*" (IntE 7) (OpE "/" (IntE 4) (IntE 2))

-- Serialise and compress with gzip, then decompress and deserialise
main = do
	let t  = encode e
	print t
	let e' = decode t
	print (e == e')
