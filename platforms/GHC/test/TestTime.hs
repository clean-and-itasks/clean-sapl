module TestTime (main) where

import Data.Time
 
main = do
	c <- getCurrentTime
	print $ toGregorian $ utctDay c 