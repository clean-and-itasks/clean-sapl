module Test64 (main) where

import Data.Word
import Data.Int
import Numeric

w64 :: Word64
w64 = 0xFFFFFFFFFFFF

w1 :: Word64
w1 = 0x1

main = do
	putStr "0x"
	putStr $ showHex (w64+w1+w1) ""
	
