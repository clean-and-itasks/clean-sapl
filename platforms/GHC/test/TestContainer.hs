module TestContainer (main) where

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Sequence as Seq

al = [(1, "one"), (2, "two"), (3, "three"), (4, "four")]

mapFold =
    foldl (\map (k, v) -> Map.insert k v map) Map.empty al

mapSet =
	Set.fromList (map snd al)
	
seq1 = Seq.fromList [1,3,4]
seq2 = Seq.fromList [6,7]
	
main = do
	print $ show (Map.insert 3 "harom" mapFold)
	print $ show (mapFold Map.! 3)
	print $ show (mapSet Set.\\ (Set.delete "two" mapSet))
	print $ show (seq1 Seq.>< seq2)
	