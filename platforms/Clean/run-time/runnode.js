var fs = require('fs');

eval(fs.readFileSync('utils.js')+'');
eval(fs.readFileSync('builtin.js')+'');
eval(fs.readFileSync('sapl.js')+'');
eval(fs.readFileSync('main.js')+'');

function run(expr){
	
	var start = new Date().getTime();

	try{
		var tmp;
		eval("tmp = "+expr+";");
		tmp = Sapl.feval(tmp);
				
		println("");
		println("-------------------------");
		println("RETURN: "+tmp);
	}catch(err){
		println("ERROR: "+err);
	}

	println("-------------------------");
	
	var end = new Date().getTime();
	var time = end - start;
	println('Execution time: ' + time);
}

run("__main()");
